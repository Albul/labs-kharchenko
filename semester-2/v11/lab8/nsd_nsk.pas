unit nsd_nsk;
interface
	function NSD(a,b:integer):integer;
	function NSK(a,b:integer):integer;
	function MAX(a,b:integer):integer;
	function MIN(a,b:integer):integer;
	procedure OPERATION_5(a1,a2,a3,a4,a5:integer; diya:string; var n:integer);
	
implementation
	function NSD(a,b:integer):integer;
	begin
		while (a<>0) and (b<>0) do begin
			if a>b then
				a:=a mod b
			else
				b:=b mod a;
		end;
		NSD:=a+b;
	end;

	function NSK(a,b:integer):integer;
	begin
		NSK:=round((a*b)/NSD(a,b));
	end;
	
	function MAX(a,b:integer):integer;
	begin
		if a>b then 
			MAX:=a
		else
			MAX:=b;
	end;

	function MIN(a,b:integer):integer;
	begin
		if a<b then
			MIN:=a
		else
			MIN:=b;
	end;

	procedure OPERATION_5(a1,a2,a3,a4,a5:integer; diya:string; var n:integer);
	begin
		if diya='nsd' then begin
			n:=NSD(a1,a2);
			if a3<>0 then n:=NSD(n,a3);
			if a4<>0 then n:=NSD(n,a4);
			if a5<>0 then n:=NSD(n,a5);
		end;

		if diya='nsk' then begin 
			n:=NSK(a1,a2);
			if a3<>0 then n:=NSK(n,a3);
			if a4<>0 then n:=NSK(n,a4);
			if a5<>0 then n:=NSK(n,a5);
		end;

		if diya='max' then begin
			n:=MAX(a1,a2);
			if a3<>0 then n:=MAX(n,a3);
			if a4<>0 then n:=MAX(n,a4);
			if a5<>0 then n:=MAX(n,a5);
		end;

		if diya='min' then begin
			n:=MIN(a1,a2);
			if a3<>0 then n:=MIN(n,a3);
			if a4<>0 then n:=MIN(n,a4);
			if a5<>0 then n:=MIN(n,a5);
		end;
	end;

begin
end.
