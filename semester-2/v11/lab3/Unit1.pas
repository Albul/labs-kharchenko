unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    GroupBox2: TGroupBox;
    Button9: TButton;
    Button10: TButton;
    Button11: TButton;
    Button12: TButton;
    Button13: TButton;
    Button14: TButton;
    Label3: TLabel;
    Label4: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Button12Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
Form1.Edit3.Text:=FloatToStr(StrToFloat(Form1.Edit1.Text)+StrToFloat(Form1.Edit2.Text));
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
Form1.Edit3.Text:=FloatToStr(StrToFloat(Form1.Edit1.Text)-StrToFloat(Form1.Edit2.Text));
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
Form1.Edit3.Text:=FloatToStr(StrToFloat(Form1.Edit1.Text)*StrToFloat(Form1.Edit2.Text));
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
if Form1.Edit2.Text<>'0' then Form1.Edit3.Text:=FloatToStr(StrToFloat(Form1.Edit1.Text)/StrToFloat(Form1.Edit2.Text))
  else showmessage('�� ���� ����� �� �����');
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  x,z,y:real;
begin
  x:=StrToFloat(Form1.Edit1.Text);
  y:=StrToFloat(Form1.Edit2.Text);
  z:=exp(y*ln(x));
  Form1.Edit3.Text:=FloatToStr(z);
end;

procedure TForm1.Button6Click(Sender: TObject);
var x,y:real;
begin
  x:=StrToFloat(Edit1.Text);
  if x>=0 then
    Form1.Edit3.Text:=FloatToStr(sqrt(StrToFloat(Form1.Edit1.Text)))
  else
    ShowMessage('������������ ��������');
end;

procedure TForm1.Button13Click(Sender: TObject);
begin
Edit1.Clear;
Edit2.Clear;
Edit3.Clear;
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
Edit3.Text:=FloatToStr(sin(StrToFloat(Edit1.Text)));
end;

procedure TForm1.Button10Click(Sender: TObject);
begin
Edit3.Text:=FloatToStr(cos(StrToFloat(Edit1.Text)));
end;

procedure TForm1.Button11Click(Sender: TObject);
var
  x,y:real;
begin
  x:=sin(StrToFloat(Edit1.Text));
  y:=cos(StrToFloat(Edit1.Text));
  if y<>0 then
    Edit3.Text:=FloatToStr(x/y)
  else
    ShowMessage('������������ ��������');
end;

procedure TForm1.Button12Click(Sender: TObject);
var
  x,y:real;
begin
  x:=sin(StrToFloat(Edit1.Text));
  y:=cos(StrToFloat(Edit1.Text));
  if x<>0 then
    Edit3.Text:=FloatToStr(y/x)
  else
    ShowMessage('������������ ��������');
end;

procedure TForm1.Button8Click(Sender: TObject);
var
  x,y,z:real;
begin
  x:=StrToFloat(Edit1.Text);
  if (x>0) then begin
    z:=ln(x);
    Edit3.Text:=FloatToStr(z);
    end
    else
    ShowMessage('����������� ��������');

end;

procedure TForm1.Button14Click(Sender: TObject);
var
  x,y:real;
begin
x:=StrToFloat(Edit1.Text);
 if x<3.5 then begin
    y:=(2*x)/(-4*x+1);
    end
  else begin
    y:=4*x*x+2*x-19;
    end;
Edit3.Text:=FloatToStr(y);
end;

procedure TForm1.Button7Click(Sender: TObject);
var
  x,y,z:real;
begin
  x:=StrToFloat(Edit1.Text);
  y:=StrToFloat(Edit2.Text);
  if ((y>0) and (y<>1) and (x>0)) then begin
    z:=ln(x)/ln(y);
    Edit3.Text:=FloatToStr(z);
    end
    else
    ShowMessage('����������� ������ ���������');
end;

end.
