unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormPaint(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
{    procedure TForm1.Timer1Timer(Sender: TObject);   }
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  x,y,j,p:integer;


implementation

{$R *.dfm}
const dx=20; dy=20;

procedure kadr1(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x-60,y);
    MoveTo(x,y);
    LineTo(x+60,y);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr2(color: TColor);
  var buf: TColor;
begin
 with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x-56,y+20);
    MoveTo(x,y);
    LineTo(x+56,y-20);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr3(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x-44,y+40);
    MoveTo(x,y);
    LineTo(x+44,y-40);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr4(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x,y+60);
    MoveTo(x,y);
    LineTo(x,y-60);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr5(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x+44,y+60-20); //4-ta 1-i
    MoveTo(x,y);
    LineTo(x-44,y-60+20);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr6(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x+56,y+60-40); //4-ta 2-i
    MoveTo(x,y);
    LineTo(x-56,y-60+40);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr7(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x+60,y); // os x+
    MoveTo(x,y);
    LineTo(x-60,y);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr8(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x+56,y-20);  //1-sha 1-i
    MoveTo(x,y);
    LineTo(x-56,y+20);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr9(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x+44,y-40);  //1-sha 2-i
    MoveTo(x,y);
    LineTo(x-44,y+40);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr10(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x,y-60);  //os y +
    MoveTo(x,y);
    LineTo(x,y+60);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr11(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x-44,y-60+20);  //os y +
    MoveTo(x,y);
    LineTo(x+44,y+60-20);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr12(color: TColor);
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    x:=round(Form1.ClientWidth/2);
    y:=round(Form1.ClientHeight/2);
    MoveTo(x,y);
    LineTo(x-56,y-60+40);  //os y +
    MoveTo(x,y);
    LineTo(x+56,y+60-40);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
x:=round(Form1.ClientWidth/2);
y:=round(Form1.ClientHeight/2);

if j<12 then j:=j+1
  else j:=1;

case j of              //������� ������� ����
1:  kadr12(form1.color);
2:  kadr1(form1.color);
3:  kadr2(form1.color);
4:  kadr3(form1.color);
5:  kadr4(form1.color);
6:  kadr5(form1.color);
7:  kadr6(form1.color);
8:  kadr7(form1.color);
9:  kadr8(form1.color);
10:  kadr9(form1.color);
11:  kadr10(form1.color);
12:  kadr11(form1.color);
end; //case


case j of       //���������� ��������� ����
  1: kadr1(clWhite);
  2: kadr2(clWhite);
  3: kadr3(clWhite);
  4: kadr4(clWhite);
  5: kadr5(clWhite);
  6: kadr6(clWhite);
  7: kadr7(clWhite);
  8: kadr8(clWhite);
  9: kadr9(clWhite);
  10: kadr10(clWhite);
  11: kadr11(clWhite);
  12: kadr12(clWhite);
end; //case
end;


procedure TForm1.FormPaint(Sender: TObject);
begin
x:=0; y:=100; j:=0;
 Form1.Color:=clNavy;
 Timer1.Interval := 70;
end;

end.









