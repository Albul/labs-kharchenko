unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs;

type
  TfFunc = class(TForm)
    procedure FormPaint(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fFunc: TfFunc;
  a,b,c,d:real;

implementation

{$R *.dfm}
uses Main;

Function f(x:real):real; // �������, ��� ������ ������� ����������
begin
 f:=a*ln(x*x+6*x);
end;

Function f2(x:real):real; // �������, ��� ������ ������� ����������
begin
 f2:=b*abs(sin(c*x+d));
end;

procedure PlotFunc1; // ������ ������ ������� �1
var
 x1,x2:real; // ������� ��������� ��������� �������
 x:real; // �������� �������
 y:real; // �������� ������� � ����� x
 dx:real; // ���������� ���������
 mx,my:real; // ������� �� ���� X � Y
 x0,y0:integer; // ����� - ������ ���������

begin
 x1:=1.001; // ������ ������� ��������� ���������
 x2:=30; // ������� ������� ��������� ���������
 dx:=0.001; // ��� ���������

 x0:=round(fFunc.ClientWidth/2);
 y0:=round(fFunc.ClientHeight/2);

 mx:=40;
 my:=40;

 with fFunc.Canvas do
 begin
 // ���������� �������
 x:=x1;
 repeat
  y:=f(x);
  Pixels[x0+Round(x*mx),y0-Round(y*my)]:=clRed;
  x:=x+dx;
 until (x>=x2);
end;
end;

procedure PlotFunc2; // ������ ������ ������� �2
var
 x1,x2:real; // ������� ��������� ��������� �������
 x:real; // �������� �������
 y:real; // �������� ������� � ����� x
 dx:real; // ���������� ���������
 mx,my:real; // ������� �� ���� X � Y
 x0,y0:integer; // ����� - ������ ���������

begin
 x1:=-20; // ������ ������� ��������� ���������
 x2:=1; // ������� ������� ��������� ���������
 dx:=0.001; // ��� ���������

 x0:=round(fFunc.ClientWidth/2);
 y0:=round(fFunc.ClientHeight/2);

 mx:=40;
 my:=40;

 with fFunc.Canvas do
 begin
 // �������� �������
 x:=x1;
 repeat
  y:=f2(x);
  Pixels[x0+Round(x*mx),y0-Round(y*my)]:=clGreen;
  x:=x+dx;
 until (x>=x2);
end;
end;

procedure PlotOsi;
var
  x1,y1:integer;
  i,j:integer;
  dx,dy:integer; //���� ����
  ldx,ldy:real; //���� ���������
  lx,ly:real;  //������ ����� �� �������
begin
  a:=Main.a;
  b:=Main.b;
  c:=Main.c;
  d:=Main.d;
  {��}
  fFunc.Canvas.Pen.Style:=psSolid;
  with fFunc.Canvas do begin
    x1:=round(fFunc.ClientWidth/2);
    y1:=round(fFunc.ClientHeight/2);
    MoveTo(x1,0);
    LineTo(x1,fFunc.ClientHeight);
    MoveTo(0,y1);
    LineTo(fFunc.ClientWidth,y1);
  end;
  {����������� ����}
  dx:=40;
  dy:=40;
  fFunc.Canvas.Pen.Style:=psDot;
  {for i := 1 to 50 do begin}
  i:=1;
  while x1-i*dx>0 do begin
    with fFunc.Canvas do begin
      MoveTo(x1+i*dx,0);
      LineTo(x1+i*dx,fFunc.ClientHeight);
      MoveTo(x1-i*dx,0);
      LineTo(x1-i*dx,fFunc.ClientHeight);
    end;
  i:=i+1;
  end;
 {������������� ����}
 {for i := 1 to 50 do begin}
 i:=1;
 while y1-i*dy>0 do begin
    with fFunc.Canvas do begin
      MoveTo(0,y1+i*dy);
      LineTo(fFunc.ClientWidth,y1+i*dy);
      MoveTo(0,y1-i*dy);
      LineTo(fFunc.ClientWidth,y1-i*dy);
      i:=i+1;
    end;
  end;
  {��������� �� �� ��}
  ldx:=1.0;
  ldy:=1.0;
  lx:=ldx;
  ly:=ldy;
  i:=1;
  fFunc.Canvas.TextOut(x1-8,y1+2,'0');
  while x1-i*dx>0 do begin
    with fFunc.Canvas do begin
      {���� ����� �� ����������� �� ��������� �� �� �� �� 4 ����� �����}
      if lx<10 then begin
        TextOut(x1-8+i*dx,y1+2,FloatToStr(lx));
        TextOut(x1-12-i*dx,y1+2,FloatToStr(-lx));
      end
      else begin
        TextOut(x1-14+i*dx,y1+2,FloatToStr(lx));
        TextOut(x1-18-i*dx,y1+2,FloatToStr(-lx));
      end;
    end;
  lx:=lx+ldx;
  i:=i+1;
  end;

  {��������� �� ��}
  i:=1;
  while y1-i*dy>0 do begin
    with fFunc.Canvas do begin
      {���� ����� �� ����������� �� ��������� �� �� �� �� 4 ����� �����}
      if ly<10 then begin
        TextOut(x1-12,y1-14+i*dy,FloatToStr(-ly));
        TextOut(x1-8,y1-14-i*dy,FloatToStr(ly));
      end
      else begin
        TextOut(x1-18,y1-14+i*dy,FloatToStr(-ly));
        TextOut(x1-14,y1-14-i*dy,FloatToStr(ly));
      end;
    end;
  ly:=ly+ldy;
  i:=i+1;
  end;

end;  //procedure


procedure TfFunc.FormPaint(Sender: TObject);
begin
PlotOsi;
PlotFunc1;
PlotFunc2;
end;

end.







fFunc.Canvas.Pen.Style:=psSolid;

with fFunc.Canvas do begin
{��������}
  x0:=(-b/2*a)*40;                                //�������� ����� ��������
  y0:=(a*(-b/2*a)*(-b/2*a)+b*(-b/2*a)+c)*(-40);  //�������� ����� ��������  -40 �� ��� Y �����������
  x2:=round(x0+x1);                               //���. ����� �������� ������� � ����� ����
  y2:=round(y0+y1);
  MoveTo(x2,y2);
  for x := x2 to 2*x2 do begin
    LineTo(x,y2-round(a*(x-x2)*(x-x2)*0.025));
  end;
  MoveTo(x2,y2);
  for x := x2 downto 0 do begin
    LineTo(x,y2-round(a*(x-x2)*(x-x2)*0.025));
  end;


  end;  //with
