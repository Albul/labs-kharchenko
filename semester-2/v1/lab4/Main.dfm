object Form1: TForm1
  Left = 229
  Top = 150
  Width = 459
  Height = 133
  Caption = #1050#1072#1083#1100#1082#1091#1083#1103#1090#1086#1088
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PopupMenu = PopupMenu1
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 296
    Top = 24
    Width = 11
    Height = 24
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 72
    Top = 0
    Width = 11
    Height = 20
    Caption = 'x:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 208
    Top = 0
    Width = 11
    Height = 20
    Caption = 'y:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 16
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 160
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 320
    Top = 24
    Width = 121
    Height = 21
    ReadOnly = True
    TabOrder = 2
  end
  object MainMenu1: TMainMenu
    Left = 344
    Top = 48
    object N1: TMenuItem
      Caption = #1060#1072#1081#1083
      object N4: TMenuItem
        Caption = #1044#1086#1076#1072#1074#1072#1085#1085#1103
        OnClick = N4Click
      end
      object N5: TMenuItem
        Caption = #1042#1110#1076#1085#1110#1084#1072#1085#1085#1103
        OnClick = N5Click
      end
      object N6: TMenuItem
        Caption = #1052#1085#1086#1078#1077#1085#1085#1103
        OnClick = N6Click
      end
      object N7: TMenuItem
        Caption = #1044#1110#1083#1077#1085#1085#1103
        OnClick = N7Click
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object sinx1: TMenuItem
        Caption = 'sin(x)'
        OnClick = sinx1Click
      end
      object cosx1: TMenuItem
        Caption = 'cos(x)'
        OnClick = cosx1Click
      end
      object F11: TMenuItem
        Caption = 'ch(x)'
        OnClick = F11Click
      end
      object N25: TMenuItem
        Caption = '-'
      end
      object N12: TMenuItem
        Caption = #1054#1095#1080#1089#1090#1080#1090#1080
        OnClick = N12Click
      end
      object N13: TMenuItem
        Caption = #1042#1080#1093#1110#1076
        OnClick = N13Click
      end
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 400
    Top = 48
    object N14: TMenuItem
      Caption = #1044#1086#1076#1072#1074#1072#1085#1085#1103
      OnClick = N14Click
    end
    object N15: TMenuItem
      Caption = #1042#1110#1076#1085#1110#1084#1072#1085#1085#1103
      OnClick = N15Click
    end
    object N16: TMenuItem
      Caption = #1052#1085#1086#1078#1077#1085#1085#1103
      OnClick = N16Click
    end
    object N17: TMenuItem
      Caption = #1044#1110#1083#1077#1085#1085#1103
      OnClick = N17Click
    end
    object N18: TMenuItem
      Caption = '-'
    end
    object sinx2: TMenuItem
      Caption = 'sin(x)'
      OnClick = sinx2Click
    end
    object cosx2: TMenuItem
      Caption = 'cos(x)'
      OnClick = cosx2Click
    end
    object F12: TMenuItem
      Caption = 'ch(x)'
      OnClick = F12Click
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object N23: TMenuItem
      Caption = #1054#1095#1080#1089#1090#1080#1090#1080
      OnClick = N23Click
    end
    object N24: TMenuItem
      Caption = #1042#1080#1093#1110#1076
      OnClick = N24Click
    end
  end
end
