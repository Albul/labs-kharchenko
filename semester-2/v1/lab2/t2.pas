program sonce;
uses crt,graph;
var 
	driver,mode:integer;
begin
	driver:=detect;
	initgraph(driver,mode,'');
	circle(500,400,200);
	line(500,600,500,200);

	{Golova}
	pieslice(500,200,0,180,50);

	{Usy}
	line(520,155,520,120);
	line(480,155,480,120);

	{}
	line(520,120,540,130);
	line(540,130,520,140);

	line(480,120,460,130);
	line(460,130,480,140);

	ellipse(400,320,0,360,20,40);
	ellipse(600,320,0,360,20,40);
	ellipse(400,500,0,360,20,40);
	ellipse(600,500,0,360,20,40);

	ellipse(500,400,90,270,20,40); 

	setfillstyle(1,4);
	floodfill(510,410,15);
	floodfill(490,310,15);

	readln;
	closegraph;
end.

