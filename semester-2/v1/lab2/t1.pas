program piram;
uses crt,graph;
var 
	driver,mode:integer;
begin
	driver:=detect;
	initgraph(driver,mode,'');
	line(400,400,600,450);
	line(600,450,650,350);
	line(650,350,400,400);

	line(450,150,400,400);
	line(450,150,600,450);
	line(450,150,650,350);

	setcolor(2);
	line(425,275,525,300);
	line(425,275,550,250);
	line(550,250,525,300);

	setfillstyle(3,2);
	floodfill(490,280,2);
	setcolor(15);
	line(450,150,600,450);

	readln;
	closegraph;
end.
