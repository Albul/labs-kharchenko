object Form1: TForm1
  Left = 0
  Top = 0
  Width = 715
  Height = 639
  VertScrollBar.ButtonSize = 1
  VertScrollBar.Margin = 1
  VertScrollBar.Range = 1
  VertScrollBar.Smooth = True
  VertScrollBar.Size = 1
  VertScrollBar.Style = ssFlat
  VertScrollBar.ThumbSize = 1
  VertScrollBar.Tracking = True
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 69
    Width = 283
    Height = 26
    Caption = 
      '1. '#1071#1082#1080#1084#1080' '#1086#1087#1077#1088#1072#1090#1086#1088#1072#1084#1080' '#1088#1077#1082#1086#1084#1077#1085#1076#1091#1102#1090#1100' '#1082#1086#1088#1080#1089#1090#1091#1074#1072#1090#1080#1089#1103' '#1074' '#1089#1090#1088#1091#1082#1090#1091#1088#1085#1086#1084#1091' '#1087 +
      #1088#1086#1075#1088#1072#1084#1091#1074#1072#1085#1085#1110'?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 48
    Top = 204
    Width = 302
    Height = 13
    Caption = '2. '#1050#1086#1083#1080' '#1074#1080#1082#1086#1085#1072#1108#1090#1100#1089#1103' '#1086#1087#1077#1088#1072#1090#1086#1088' '#1087#1110#1089#1083#1103' '#1082#1083#1102#1095#1086#1074#1086#1075#1086' '#1089#1083#1086#1074#1072' else?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label3: TLabel
    Left = 48
    Top = 326
    Width = 91
    Height = 13
    Caption = '3. '#1054#1087#1077#1088#1072#1090#1086#1088' if '#1094#1077':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 435
    Top = 119
    Width = 241
    Height = 13
    Caption = '5. '#1071#1082#1086#1075#1086' '#1090#1080#1087#1091' '#1087#1086#1074#1080#1085#1085#1072' '#1073#1091#1090#1080' '#1091#1084#1086#1074#1072' '#1086#1087#1077#1088#1072#1090#1086#1088#1072' if&'
  end
  object Label5: TLabel
    Left = 435
    Top = 263
    Width = 260
    Height = 26
    Caption = '6. '#1057#1082#1110#1083#1100#1082#1080' '#1088#1072#1079#1110#1074' '#1074#1080#1082#1086#1085#1072#1108#1090#1100#1089#1103' '#1079#1086#1074#1085#1110#1096#1085#1110#1081' '#1086#1087#1077#1088#1072#1090#1086#1088' if '#1091' '#1074#1080#1088#1072#1079#1110':'
    WordWrap = True
  end
  object Label6: TLabel
    Left = 311
    Top = 528
    Width = 3
    Height = 13
  end
  object Label7: TLabel
    Left = 464
    Top = 61
    Width = 140
    Height = 13
    Caption = #1042#1074#1077#1076#1110#1090#1100' '#1096#1082#1072#1083#1091' '#1086#1094#1110#1085#1102#1074#1072#1085#1085#1103':'
  end
  object Label8: TLabel
    Left = 48
    Top = 471
    Width = 144
    Height = 91
    Caption = 
      'a:= 900;                                      if a>10           ' +
      '                               then if a>100                    ' +
      '           then if a>1000                              then z:= ' +
      '1                            else z:= -1                   else ' +
      'z:= 10;'
    WordWrap = True
  end
  object Label9: TLabel
    Left = 435
    Top = 295
    Width = 102
    Height = 78
    Caption = 
      'i:=0;                       mit: if true then   begin           ' +
      '           i:=i+1;                     if i<12 then goto mit; en' +
      'd;'
    WordWrap = True
  end
  object Label10: TLabel
    Left = 48
    Top = 456
    Width = 180
    Height = 13
    Caption = '4. '#1071#1082#1086#1075#1086' '#1079#1085#1072#1095#1077#1085#1085#1103' '#1085#1072#1073#1091#1076#1077' '#1079#1084#1110#1085#1085#1072' z?'
  end
  object Label11: TLabel
    Left = 192
    Top = 8
    Width = 277
    Height = 19
    Caption = #1058#1077#1089#1090#1091#1074#1072#1085#1085#1103' '#1087#1086' '#1090#1077#1084#1110' "'#1086#1087#1077#1088#1072#1090#1086#1088' if"'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 48
    Top = 101
    Width = 185
    Height = 97
    TabOrder = 0
    object CheckBox1: TCheckBox
      Left = 3
      Top = 18
      Width = 97
      Height = 17
      Caption = 'if'
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 3
      Top = 41
      Width = 97
      Height = 17
      Caption = 'goto'
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 3
      Top = 64
      Width = 150
      Height = 17
      Caption = 'while'
      TabOrder = 2
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 48
    Top = 223
    Width = 185
    Height = 97
    Items.Strings = (
      #1050#1086#1083#1080' '#1091#1084#1086#1074#1072' '#1087#1086#1074#1077#1088#1090#1072#1108' true'
      #1050#1086#1083#1080' '#1091#1084#1086#1074#1072' '#1087#1086#1074#1077#1088#1090#1072#1108' false'
      #1050#1086#1083#1080' '#1091#1084#1086#1074#1072' '#1087#1086#1074#1077#1088#1090#1072#1108' '#1085#1091#1083#1100)
    TabOrder = 1
  end
  object RadioGroup2: TRadioGroup
    Left = 48
    Top = 345
    Width = 218
    Height = 105
    Items.Strings = (
      #1054#1087#1077#1088#1072#1090#1086#1088' '#1074#1080#1073#1086#1088#1091
      #1054#1087#1077#1088#1072#1090#1086#1088' '#1088#1086#1079#1075#1072#1083#1091#1078#1077#1085#1085#1103
      #1054#1087#1077#1088#1072#1090#1086#1088' '#1073#1077#1079#1091#1084#1086#1074#1085#1086#1075#1086' '#1087#1077#1088#1077#1093#1086#1076#1091)
    TabOrder = 2
  end
  object RadioGroup3: TRadioGroup
    Left = 435
    Top = 138
    Width = 185
    Height = 105
    Items.Strings = (
      'integer'
      'real'
      'true'
      'boolean')
    TabOrder = 3
  end
  object Edit1: TEdit
    Left = 48
    Top = 576
    Width = 121
    Height = 21
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    Text = #1042#1074#1077#1076#1110#1090#1100' '#1074#1110#1076#1087#1086#1074#1110#1076#1100
    OnClick = Edit1Click
  end
  object Button1: TButton
    Left = 311
    Top = 572
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 5
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 610
    Top = 58
    Width = 47
    Height = 21
    TabOrder = 6
    Text = '5'
  end
  object RadioGroup4: TRadioGroup
    Left = 435
    Top = 379
    Width = 185
    Height = 105
    Items.Strings = (
      #1054#1076#1080#1085' '#1088#1072#1079
      #1053#1077#1089#1082#1110#1085#1095#1077#1085#1085#1091' '#1082#1110#1083#1100#1082#1110#1089#1090#1100' '#1088#1072#1079#1110#1074
      #1053#1077' '#1088#1072#1079#1091' '#1085#1077' '#1074#1080#1082#1086#1085#1072#1108#1090#1100#1089#1103
      '12 '#1088#1072#1079#1110#1074
      '13 '#1088#1072#1079#1110#1074)
    TabOrder = 7
  end
  object Button2: TButton
    Left = 416
    Top = 572
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1080
    TabOrder = 8
    OnClick = Button2Click
  end
end
