unit compl;
interface
	type complex=record
		x,y:real; 	{x - dijsna; y - uyavna}
	end;

	procedure input_complex(n:integer; var z1:complex);
	procedure output_complex(z1:complex);
	procedure dod_complex(z1,z2:complex; var z3:complex);
	procedure vidnim_complex(z1,z2:complex; var z3:complex);
	procedure mnozh_complex(z1,z2:complex; var z3:complex);
	procedure dil_complex(z1,z2:complex; var z3:complex);	
	procedure stepin_complex(z1:complex; n:integer; var z3:complex);
	

implementation
	procedure input_complex(n:integer; var z1:complex);
	begin
		writeln('Vvedit" complexne chyslo z',n);
		read(z1.x);
		read(z1.y);
	end;

	procedure output_complex(z1:complex);
	begin
		if z1.y>0 then write('(',z1.x:5:2,' + i*',abs(z1.y):5:2,')')
			else write('(',z1.x:5:2,' - i*',abs(z1.y):5:2,')');
	end;

	procedure dod_complex(z1,z2:complex; var z3:complex);
	begin
		z3.x:=z1.x+z2.x;
		z3.y:=z1.y+z2.y;
	end;
	
	procedure vidnim_complex(z1,z2:complex; var z3:complex);
	begin
		z3.x:=z1.x-z2.x;
		z3.y:=z1.y-z2.y;
	end;
	
	procedure mnozh_complex(z1,z2:complex; var z3:complex);
	begin
		z3.x:=z1.x*z2.x-z1.y*z2.y;
		z3.y:=z1.x*z2.y+z2.x*z1.y;
	end;

	procedure dil_complex(z1,z2:complex; var z3:complex);
	begin
		z3.x:=(z1.x*z2.x+z1.y*z2.y)/(sqr(z2.x)+sqr(z2.y));
		z3.y:=(z2.x*z1.y-z1.x*z2.y)/(sqr(z2.x)+sqr(z2.y));
	end;

	procedure stepin_complex(z1:complex; n:integer; var z3:complex);
	var 
		poch:complex;
		i:integer;
	begin
		poch:=z1;
		z3:=z1;
		i:=1;
		while i<n do begin
			mnozh_complex(z3,poch,z3);
			i:=i+1;
		end;
	end;

begin
end.
