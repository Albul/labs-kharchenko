object Form1: TForm1
  Left = 234
  Top = 156
  Width = 589
  Height = 330
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 328
    Top = 40
    Width = 11
    Height = 24
    Caption = '='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 80
    Top = 16
    Width = 9
    Height = 16
    Caption = 'x:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 240
    Top = 16
    Width = 10
    Height = 16
    Caption = 'y:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Edit1: TEdit
    Left = 32
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 192
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object Edit3: TEdit
    Left = 352
    Top = 40
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 40
    Top = 96
    Width = 217
    Height = 105
    Caption = #1040#1088#1080#1092#1084#1077#1090#1080#1095#1085#1110' '#1086#1087#1077#1088#1072#1094#1110#1111
    TabOrder = 3
    object Button1: TButton
      Left = 16
      Top = 24
      Width = 75
      Height = 25
      Caption = '+'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 120
      Top = 24
      Width = 75
      Height = 25
      Caption = '-'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Button3: TButton
      Left = 16
      Top = 64
      Width = 75
      Height = 25
      Caption = '*'
      TabOrder = 2
      OnClick = Button3Click
    end
    object Button4: TButton
      Left = 120
      Top = 64
      Width = 75
      Height = 25
      Caption = '/'
      TabOrder = 3
      OnClick = Button4Click
    end
  end
  object GroupBox2: TGroupBox
    Left = 280
    Top = 96
    Width = 201
    Height = 113
    Caption = #1058#1088#1080#1075#1086#1085#1086#1084#1077#1090#1088#1080#1095#1085#1110' '#1092#1091#1085#1082#1094#1110#1111
    TabOrder = 4
    object Button9: TButton
      Left = 16
      Top = 32
      Width = 75
      Height = 25
      Caption = 'sin(x)'
      TabOrder = 0
      OnClick = Button9Click
    end
    object Button10: TButton
      Left = 112
      Top = 32
      Width = 75
      Height = 25
      Caption = 'cos(x)'
      TabOrder = 1
      OnClick = Button10Click
    end
    object Button5: TButton
      Left = 16
      Top = 72
      Width = 75
      Height = 25
      Caption = 'ch(x)'
      TabOrder = 2
      OnClick = Button5Click
    end
  end
  object Button13: TButton
    Left = 496
    Top = 104
    Width = 75
    Height = 25
    Caption = 'CE'
    TabOrder = 5
    OnClick = Button13Click
  end
end
