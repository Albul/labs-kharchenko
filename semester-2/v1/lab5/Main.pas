unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Edit1: TEdit;
    Edit4: TEdit;
    Label1: TLabel;
    Label4: TLabel;
    Button1: TButton;
    fImage: TImage;

    procedure Button1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  a,b,c,d:real;

  Function ff(x:real):real;
  procedure PlotFuncImage;
  procedure PlotOsiImage;
  procedure ImagePaint;
implementation

uses Unit2;

{$R *.dfm}
Function ff(x:real):real; // �������, ��� ������ ������� ����������
begin
 ff:=a*ln(abs(x+3))/(x*x+d);
end;

procedure PlotFuncImage; // ������ ������ ������� #1
var
 x1,x2:real; // ������� ���� ��������� �������
 x:real; // �������� �������
 y:real; // �������� ������� � ����� �
 dx:real; // ������ ���������
 mx,my:real; // ������� �� ���� X � Y
 x0,y0:integer; // ����� - ������� ���������

begin
 x1:=-50; // ����� ������� �������� ���������
 x2:=50; // ������ ������� �������� ���������
 dx:=0.01; // ���� ���������

 x0:=round(Form1.fImage.ClientWidth/2);
 y0:=round(Form1.fImage.ClientHeight/2);

 mx:=40;
 my:=40;

 with Form1.fImage.Canvas do
 begin
  // �������� �������
 x:=x1;
 repeat
  y:=ff(x);
  Pixels[x0+Round(x*mx),y0-Round(y*my)]:=clRed;
  x:=x+dx;
 until (x>=x2);
end;
end;

procedure PlotOsiImage;
var
  x1,y1:integer;
  i,j:integer;
  dx,dy:integer; //���� ����
  ldx,ldy:real; //���� ���������
  lx,ly:real;  //������ ����� �� �������
begin

  {��}
  Form1.fImage.Canvas.Pen.Style:=psSolid;
  with Form1.fImage.Canvas do begin
    x1:=round(Form1.fImage.ClientWidth/2);
    y1:=round(Form1.fImage.ClientHeight/2);
    MoveTo(x1,0);
    LineTo(x1,Form1.fImage.ClientHeight);
    MoveTo(0,y1);
    LineTo(Form1.fImage.ClientWidth,y1);
  end;
  {����������� ����}
  dx:=40;
  dy:=40;
  Form1.fImage.Canvas.Pen.Style:=psDot;
  {for i := 1 to 50 do begin}
  i:=1;
  while x1-i*dx>0 do begin
    with Form1.fImage.Canvas do begin
      MoveTo(x1+i*dx,0);
      LineTo(x1+i*dx,Form1.fImage.ClientHeight);
      MoveTo(x1-i*dx,0);
      LineTo(x1-i*dx,Form1.fImage.ClientHeight);
    end;
  i:=i+1;
  end;
 {������������� ����}
 {for i := 1 to 50 do begin}
 i:=1;
 while y1-i*dy>0 do begin
    with Form1.fImage.Canvas do begin
      MoveTo(0,y1+i*dy);
      LineTo(Form1.fImage.ClientWidth,y1+i*dy);
      MoveTo(0,y1-i*dy);
      LineTo(Form1.fImage.ClientWidth,y1-i*dy);
      i:=i+1;
    end;
  end;
  {��������� �� �� ��}
  ldx:=1.0;
  ldy:=1.0;
  lx:=ldx;
  ly:=ldy;
  i:=1;
  Form1.fImage.Canvas.TextOut(x1-8,y1+2,'0');
  while x1-i*dx>0 do begin
    with Form1.fImage.Canvas do begin
      {���� ����� �� ����������� �� ��������� �� �� �� �� 4 ����� �����}
      if lx<10 then begin
        TextOut(x1-8+i*dx,y1+2,FloatToStr(lx));
        TextOut(x1-12-i*dx,y1+2,FloatToStr(-lx));
      end
      else begin
        TextOut(x1-14+i*dx,y1+2,FloatToStr(lx));
        TextOut(x1-18-i*dx,y1+2,FloatToStr(-lx));
      end;
    end;
  lx:=lx+ldx;
  i:=i+1;
  end;

  {��������� �� ��}
  i:=1;
  while y1-i*dy>0 do begin
    with Form1.fImage.Canvas do begin
      {���� ����� �� ����������� �� ��������� �� �� �� �� 4 ����� �����}
      if ly<10 then begin
        TextOut(x1-12,y1-14+i*dy,FloatToStr(-ly));
        TextOut(x1-8,y1-14-i*dy,FloatToStr(ly));
      end
      else begin
        TextOut(x1-18,y1-14+i*dy,FloatToStr(-ly));
        TextOut(x1-14,y1-14-i*dy,FloatToStr(ly));
      end;
    end;
  ly:=ly+ldy;
  i:=i+1;
  end;

end;  //procedure


procedure ImagePaint;
begin
PlotOsiImage;
PlotFuncImage;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  a:=StrToFloat(Edit1.Text);
  d:=StrToFloat(Edit4.Text);
  {fImage.Canvas.Brush.Color:=clWhite;}
  fImage.Canvas.FloodFill(10, 10, clBlack, fsBorder);
  ImagePaint;
  fFunc.ShowModal;
end;





end.
