unit bigSet;
interface

        const
                //����i� �������
                SIZE_SET = 1000;
        type
                //�।�⠢����� �������
                //(�� i-� ������� �������� ������i � i-� ������� ������ ��ᨢ�
                //���i���� true, i���� false
                setArr = array[0..SIZE_SET] of boolean;

        //��楤�� ������� ������� (n - ����� �������)
        procedure inputSet(n:integer; var set1:setArr);

        //��楤�� ��������� ������� (n - ����� �������)
        procedure outputSet(n:integer; set1:setArr);

        //��ॢiઠ � �������� element ������i set1
        function isIn(element:integer; set1:setArr):boolean;

        //��󤭠��� ���� ������
	procedure unionSet(set1, set2:setArr; var setOutput:setArr);

        //��楤�� ����i�� ���� ������
        procedure intersectionSet(set1, set2:setArr; var setOutput:setArr);

        //��楤�� �i����i ���� ������
        procedure differenceSet(set1, set2:setArr; var setOutput:setArr);

        //�-� ��ॢiન � � ������� set1 �i��������� ������� set2
        function subSet(set1, set2:setArr):boolean;


implementation

        function isIn(element:integer; set1:setArr):boolean;
        begin
                isIn := set1[element];

        end;


        procedure inputSet(n:integer; var set1:setArr);
        var number,i:integer;
	begin
                writeln('Enter the number of elements of the set',n,':');
                readln(number);
		writeln('Enter elements of the set',n,':');

                while number>0 do begin
                        readln(i);
                        set1[i] := true;
                        number := number - 1;
                end;

	end;


        procedure outputSet(n:integer; set1:setArr);
        var i:integer;
        begin
                writeln('Elements of set',n,':');

                //�������� ��i ������� ����� �������
                for i:=0 to SIZE_SET do
                        if set1[i] then write(i,' ');
                writeln();
        end;



	procedure unionSet(set1,set2:setArr; var setOutput:setArr);
        var i:integer;
        begin
                //��ॡi�� ��i ������� ��i���� ������ �� �������
                //�������� �砡 ���i� i� ��� � ����� ����� ������� � १������� �������
                for i:=0 to SIZE_SET do
                    if (set1[i] or set2[i]) then setOutput[i] := true;
        end;



        procedure intersectionSet(set1, set2:setArr; var setOutput:setArr);
        var i:integer;
        begin
              //��� ������� �������� ���� ��i���� ��������, � �����
              //���� � १������� �������
              for i:=0 to SIZE_SET do
                    if (set1[i] and set2[i]) then setOutput[i] := true;
        end;

        procedure differenceSet(set1, set2:setArr; var setOutput:setArr);
        var i:integer;
        begin
                //��� ������� �������� ����i� ������i i �����᭮
                //�� �������� ���i� ������i � ����� ���� � १������� �������
                for i:=0 to SIZE_SET do
                        if (set1[i] and not(set2[i])) then setOutput[i] := true;
        end;


        function subSet(set1, set2:setArr):boolean;
        var i:integer;
        begin
                //�ਯ��⨬� � set1 � �i��������� set2
                subSet := true;
                //���i ��ॣ�鸞� ��i ������� ����� ������, �� ���������� ⠪��
                //������� � ������i set1, 直� �� �������� ������i set2, � �������
                //false, ⮡� � set1 �� � �i��������� set2
                for i:=0 to SIZE_SET do
                        if (set1[i] and not(set2[i])) then begin
                                subSet := false;
                                break;
                        end;

        end;
begin
end.
