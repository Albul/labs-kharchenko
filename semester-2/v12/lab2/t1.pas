uses graph;
var er,grd,grm:integer;
begin
        grd:=detect;
        initgraph(grd,grm,'');
        er:=graphresult;
        if er<>grok then begin
                writeln('graph error');
                halt(er);
        end;

        {tulub}
        ellipse(350,350,0,360,10,110);
        {golova}
        circle(350,225,15);

        {prave uho}
        line(358,213,362,203);
        circle(364,200,3);

        {live uho}
        line(342,213,338,203);
        circle(336,200,3);


        {prave verhne krylo}
        line(358,280,430,190);
        line(360,300,460,260);
        line(430,190,460,260);


        {live verhne krylo}
        line(343,280,271,190);
        line(340,300,240,260);
        line(271,190,240,260);

        {prave nyzhne krylo}
        line(360,310,480,360);
        line(360,360,430,450);
        line(480,360,430,450);


        {live nyzhne krylo}
        line(341,310,225,360);
        line(340,360,275,450);
        line(225,360,275,450);

        ellipse(285,250,0,360,10,20);
        ellipse(415,250,0,360,10,20);
        ellipse(275,395,0,360,10,20);
        ellipse(430,395,0,360,10,20);

        ellipse(390,350,0,360,20,5);
        ellipse(306,350,0,360,20,5);

        setfillstyle(1,4);
        floodfill(360,280,15);
        floodfill(340,280,15);
        floodfill(362,320,15);
        floodfill(335,315,15);


        readln;
        closegraph;
end.
