unit kramar;
interface
	const r=1001;
	type matrix=array[1..3,1..r] of real;

	procedure input_matrix(var A:matrix);
	procedure output_matrix(A:matrix);
	procedure determinant2(A:matrix; var d:real);	
	procedure determinant3(A:matrix; var d:real);
	procedure kramars(A:matrix; var x1,x2,x3:real);
	
implementation
	var i,j:integer;

	procedure input_matrix(var A:matrix); 
	var n,m:integer;
		x:real;
	begin
		writeln('Vvedit" rozmirnist" rozshyrenoi matryci rivnyannya (napryklad: 3 4):');
		readln(n,m);
		A[1,r-1]:=n;
		A[1,r]:=m;
		writeln('Zadajte matrycyu:');
		for i:=1 to n do begin
			for j:=1 to m do begin 
				read(x);
				A[i,j]:=x;
			end;
			writeln;
		end;
	end;


	procedure output_matrix(A:matrix); 
	var n,m:integer;	
	begin
		n:=trunc(A[1,r-1]);
		m:=trunc(A[1,r]);
		for i:=1 to n do begin
			for j:=1 to m do
				write(A[i,j]:5:2,' ');
			writeln;
		end;
	end;

	procedure determinant2(A:matrix; var d:real);	
	begin
		d:=A[1,1]*A[2,2]-A[2,1]*A[1,2];
	end;

	procedure determinant3(A:matrix; var d:real);	
	begin
		d:=A[1,1]*A[2,2]*A[3,3]+A[1,2]*A[2,3]*A[3,1]+A[2,1]*A[3,2]*A[1,3]-A[1,3]*A[2,2]*A[3,1]-A[2,3]*A[3,2]*A[1,1]-A[2,1]*A[1,2]*A[3,3];
	end;

	procedure kramars(A:matrix; var x1,x2,x3:real);
	var d,d1,d2,d3:real;
		A1,A2,A3:matrix;
		B:integer;
	begin		
		A1:=A;
		A2:=A;
		A3:=A;
		B:=trunc(A[1,r]);
		A1[1,1]:=A[1,B];
		A1[2,1]:=A[2,B];
		A1[3,1]:=A[3,B];

		A2[1,2]:=A[1,B];
		A2[2,2]:=A[2,B];
		A2[3,2]:=A[3,B];

		A3[1,3]:=A[1,B];
		A3[2,3]:=A[2,B];
		A3[3,3]:=A[3,B];

		if A[1,r-1]=2 then begin
		 	determinant2(A,d);
		 	determinant2(A1,d1);
		 	determinant2(A2,d2);
		end
		else begin
			determinant3(A,d);
			determinant3(A1,d1);
			determinant3(A2,d2);
			determinant3(A3,d3);
		end;

		if (d=0) and ((d1<>0) or (d2<>0) or (d3<>0)) then begin 
		 	writeln('Systema ne sumistna');
			exit;
		end;
		if (d=0) and (d1=0) and (d2=0) and (d3=0) then begin
		 	writeln('Potribne dodatkove doslidzhennya'); 
			exit;
		end;

		if A[1,r-1]=2 then begin
		 	x1:=d1/d;
			x2:=d2/d;
			writeln('x1 = ',x1:5:2,' x2 = ',x2:5:2);
		end
		else begin
			x1:=d1/d;
			x2:=d2/d;
			x3:=d3/d;
			writeln('x1 = ',x1:5:2,' x2 = ',x2:5:2,' x3 = ',x3:5:2);	
		end;
	end;
	

begin
end.
