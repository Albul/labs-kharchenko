object Form1: TForm1
  Left = 293
  Top = 82
  Width = 715
  Height = 726
  VertScrollBar.ButtonSize = 1
  VertScrollBar.Margin = 1
  VertScrollBar.Range = 1
  VertScrollBar.Smooth = True
  VertScrollBar.Size = 1
  VertScrollBar.Style = ssFlat
  VertScrollBar.ThumbSize = 1
  VertScrollBar.Tracking = True
  AutoScroll = False
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 53
    Width = 297
    Height = 26
    Caption = 
      '1. '#1071#1082#1110' '#1082#1083#1102#1095#1086#1074#1110' '#1089#1083#1086#1074#1072' '#1086#1073#1086#1074#39#1103#1079#1082#1086#1074#1086' '#1087#1086#1074#1080#1085#1085#1110' '#1073#1091#1090#1080' '#1087#1088#1080#1089#1091#1090#1085#1110#1084#1080' '#1074' '#1089#1090#1088#1091#1082 +
      #1090#1091#1088#1110' '#1084#1086#1076#1091#1083#1103'?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 48
    Top = 188
    Width = 457
    Height = 26
    Caption = 
      '2. '#1055#1110#1089#1083#1103' '#1103#1082#1086#1075#1086' '#1082#1083#1102#1095#1086#1074#1086#1075#1086' '#1089#1083#1086#1074#1072' '#1081#1076#1077' '#1087#1077#1088#1077#1083#1110#1082' '#1084#1086#1076#1091#1083#1110#1074' '#1103#1082#1110' '#1074#1080#1082#1086#1088#1080#1089#1090#1086 +
      #1074#1091#1102#1090#1100#1089#1103' '#1091' '#1087#1088#1086#1075#1088#1072#1084#1084#1110'?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label3: TLabel
    Left = 48
    Top = 302
    Width = 290
    Height = 13
    Caption = '3. '#1071#1082#1097#1086' '#1074' '#1087#1088#1086#1075#1088#1072#1084#1084#1110' '#1087#1110#1076#1082#1083#1102#1095#1077#1085#1110' '#1084#1086#1076#1091#1083#1110' '#1085#1072#1089#1090#1091#1087#1085#1080#1084' '#1095#1080#1085#1086#1084':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 43
    Top = 519
    Width = 342
    Height = 52
    Caption = 
      '5. '#1071#1082#1097#1086' '#1091' '#1087#1088#1086#1075#1088#1072#1084#1110' '#1073#1091#1083#1086' '#1087#1110#1076#39#1108#1076#1085#1072#1085#1086' '#1084#1086#1076#1091#1083#1100' mod1, '#1072' '#1074' '#1094#1100#1086#1084#1091' '#1084#1086#1076#1091#1083#1110 +
      ' '#1073#1091#1083#1086' '#1086#1075#1086#1083#1086#1096#1077#1085#1086' '#1091' '#1088#1086#1079#1076#1110#1083#1110' implementation '#1079#1084#1110#1085#1085#1110' b,c. '#1058#1072#1082#1086#1078' '#1074' '#1076#1072#1085 +
      #1086#1084#1091' '#1084#1086#1076#1091#1083#1110' '#1087#1110#1076#1108#1076#1085#1091#1108#1090#1100#1089#1103' '#1084#1086#1076#1091#1083#1100' mod2, '#1074' '#1103#1082#1086#1084#1091' '#1091' '#1088#1086#1079#1076#1110#1083#1110' interface' +
      ' '#1086#1075#1086#1083#1086#1096#1077#1085#1086' '#1079#1084#1110#1085#1085#1110' k,l. '#1071#1082#1110' '#1079#1084#1110#1085#1085#1110' '#1073#1091#1076#1091#1090#1100' '#1076#1086#1089#1090#1091#1087#1085#1110' '#1091' '#1087#1088#1086#1075#1088#1072#1084#1084#1110'?'
    WordWrap = True
  end
  object Label5: TLabel
    Left = 51
    Top = 335
    Width = 342
    Height = 39
    Caption = 
      #1059' '#1082#1086#1078#1085#1086#1084#1091' '#1079' '#1084#1086#1076#1091#1083#1110#1074' '#1108' '#1086#1075#1086#1083#1086#1096#1077#1085#1072' '#1090#1080#1087#1110#1079#1086#1074#1072#1085#1072' '#1082#1086#1085#1089#1090#1072#1085#1090#1072' d '#1074' '#1088#1086#1079#1076#1110#1083#1110 +
      ' interface, '#1087#1088#1080' '#1095#1086#1084#1091' '#1074' '#1084#1086#1076#1091#1083#1110' mod1 d=12, '#1074' mod2 d=1, '#1074' mod3 d=0.' +
      ' '#1071#1082#1080#1084' '#1073#1091#1076#1077' '#1079#1085#1072#1095#1077#1085#1085#1103' '#1082#1086#1085#1089#1090#1072#1085#1090#1080' d '#1074' '#1087#1088#1086#1075#1088#1072#1084#1084#1110'? '
    WordWrap = True
  end
  object Label6: TLabel
    Left = 525
    Top = 568
    Width = 43
    Height = 13
  end
  object Label7: TLabel
    Left = 464
    Top = 61
    Width = 140
    Height = 13
    Caption = #1042#1074#1077#1076#1110#1090#1100' '#1096#1082#1072#1083#1091' '#1086#1094#1110#1085#1102#1074#1072#1085#1085#1103':'
  end
  object Label9: TLabel
    Left = 51
    Top = 319
    Width = 118
    Height = 18
    Caption = 'uses mod1,mod2,mod3;'
    WordWrap = True
  end
  object Label10: TLabel
    Left = 48
    Top = 408
    Width = 276
    Height = 13
    Caption = '4. '#1055#1110#1089#1083#1103' '#1082#1083#1102#1095#1086#1074#1086#1075#1086' '#1089#1083#1086#1074#1072' implementation '#1091' '#1084#1086#1076#1091#1083#1110' '#1081#1076#1077':'
  end
  object Label11: TLabel
    Left = 192
    Top = 8
    Width = 239
    Height = 19
    Caption = #1058#1077#1089#1090#1091#1074#1072#1085#1085#1103' '#1087#1086' '#1090#1077#1084#1110' "'#1084#1086#1076#1091#1083#1110'"'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GroupBox1: TGroupBox
    Left = 48
    Top = 85
    Width = 185
    Height = 92
    TabOrder = 0
    object CheckBox1: TCheckBox
      Left = 3
      Top = 18
      Width = 97
      Height = 17
      Caption = 'interface'
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 3
      Top = 41
      Width = 97
      Height = 17
      Caption = 'implementation'
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 3
      Top = 64
      Width = 150
      Height = 17
      Caption = 'begin'
      TabOrder = 2
    end
    object CheckBox4: TCheckBox
      Left = 107
      Top = 16
      Width = 97
      Height = 17
      Caption = 'unit'
      TabOrder = 3
    end
    object CheckBox5: TCheckBox
      Left = 107
      Top = 40
      Width = 97
      Height = 17
      Caption = 'end.'
      TabOrder = 4
    end
    object CheckBox6: TCheckBox
      Left = 107
      Top = 64
      Width = 97
      Height = 17
      Caption = 'program'
      TabOrder = 5
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 48
    Top = 207
    Width = 185
    Height = 82
    Items.Strings = (
      'var'
      'uses'
      'const'
      'type')
    TabOrder = 1
  end
  object RadioGroup2: TRadioGroup
    Left = 48
    Top = 425
    Width = 218
    Height = 80
    Items.Strings = (
      #1030#1085#1090#1077#1088#1092#1077#1081#1089#1085#1072' '#1095#1072#1089#1090#1080#1085#1072
      #1056#1077#1072#1083#1110#1079#1072#1094#1110#1081#1085#1072' '#1095#1072#1089#1090#1080#1085#1072
      #1041#1083#1086#1082' '#1110#1085#1110#1094#1110#1072#1083#1110#1079#1072#1094#1110#1111)
    TabOrder = 2
  end
  object RadioGroup3: TRadioGroup
    Left = 43
    Top = 578
    Width = 185
    Height = 79
    Items.Strings = (
      'a,b'
      'k,l'
      #1059#1089#1110' '#1085#1072#1079#1074#1072#1085#1110
      #1046#1086#1076#1085#1086#1111' '#1079' '#1085#1072#1079#1074#1072#1085#1080#1093)
    TabOrder = 3
  end
  object Edit1: TEdit
    Left = 48
    Top = 376
    Width = 121
    Height = 21
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    Text = #1042#1074#1077#1076#1110#1090#1100' '#1074#1110#1076#1087#1086#1074#1110#1076#1100
    OnClick = Edit1Click
  end
  object Button1: TButton
    Left = 463
    Top = 628
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 5
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 610
    Top = 58
    Width = 47
    Height = 21
    TabOrder = 6
    Text = '5'
  end
  object Button2: TButton
    Left = 552
    Top = 628
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1080
    TabOrder = 7
    OnClick = Button2Click
  end
end
