unit paralelogram;
interface
	procedure perumetr(x1,y1,x2,y2,x3,y3,x4,y4:integer; var p:real);
	procedure vysota(x1,y1,x2,y2,x3,y3,x4,y4:integer; var h:real);
	procedure ploscha(x1,y1,x2,y2,x3,y3,x4,y4:integer; var s:real);

implementation
	procedure perumetr(x1,y1,x2,y2,x3,y3,x4,y4:integer; var p:real);
	var a,b,c,d:real;
	begin
		a:=sqrt(sqr(x1-x2)+sqr(y1-y2));
		b:=sqrt(sqr(x2-x3)+sqr(y2-y3));
		c:=sqrt(sqr(x4-x3)+sqr(y4-y3));
		d:=sqrt(sqr(x1-x4)+sqr(y1-y4));
		
		p:=a+b+c+d;
	end;
	
	procedure vysota(x1,y1,x2,y2,x3,y3,x4,y4:integer; var h:real);
	begin	
		h:=abs(x2*(y4-y1)+y2*(x1-x4)-x1*y4+y1*x4)/sqrt(sqr(y4-y1)+sqr(x1-x4));
	end;

	procedure ploscha(x1,y1,x2,y2,x3,y3,x4,y4:integer; var s:real);
	var d,h:real;
	begin
		d:=sqrt(sqr(x1-x4)+sqr(y1-y4));
		h:=abs(x2*(y4-y1)+y2*(x1-x4)-x1*y4+y1*x4)/sqrt(sqr(y4-y1)+sqr(x1-x4));
		s:=d*h;
	end;


begin
end.
