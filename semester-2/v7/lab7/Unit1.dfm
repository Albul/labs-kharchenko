object Form1: TForm1
  Left = 215
  Top = 90
  Width = 715
  Height = 753
  VertScrollBar.ButtonSize = 1
  VertScrollBar.Margin = 1
  VertScrollBar.Range = 1
  VertScrollBar.Smooth = True
  VertScrollBar.Size = 1
  VertScrollBar.Style = ssFlat
  VertScrollBar.ThumbSize = 1
  VertScrollBar.Tracking = True
  AutoScroll = False
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 61
    Width = 105
    Height = 20
    Caption = '1. '#1047#1072#1087#1080#1089' '#1076#1086#1079#1074#1086#1083#1103#1108':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label2: TLabel
    Left = 48
    Top = 164
    Width = 241
    Height = 21
    Caption = '2. '#1047#1072#1087#1080#1089'  '#1085#1077' '#1084#1086#1078#1085#1072' '#1079#1072#1076#1072#1090#1080' '#1079' '#1076#1086#1087#1086#1084#1086#1075#1086#1102':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    WordWrap = True
  end
  object Label3: TLabel
    Left = 48
    Top = 286
    Width = 393
    Height = 13
    Caption = 
      '3. '#1047' '#1076#1086#1087#1086#1084#1086#1075#1086#1102' '#1103#1082#1086#1075#1086' '#1082#1083#1102#1095#1086#1074#1086#1075#1086' '#1089#1083#1086#1074#1072' '#1084#1086#1078#1085#1072' '#1086#1075#1086#1083#1086#1089#1080#1090#1080' '#1079#1084#1110#1085#1085#1091' '#1090#1080#1087#1091 +
      ' '#1079#1072#1087#1080#1089'?:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 51
    Top = 455
    Width = 182
    Height = 130
    Caption = 
      'var zap1: record x,y: real;                                     ' +
      '         i,j,k: integer;                                        ' +
      'flag: boolean;                                      s: set of '#39'a' +
      #39'..'#39'z'#39';                               a: array[1..100] of byte; ' +
      '                   data: record  day:1..31;                     ' +
      '           month: 1..12;                                       y' +
      'ear: 1900..2100;                                end;            ' +
      '                             end;'
    WordWrap = True
  end
  object Label6: TLabel
    Left = 528
    Top = 616
    Width = 40
    Height = 13
  end
  object Label7: TLabel
    Left = 464
    Top = 61
    Width = 140
    Height = 13
    Caption = #1042#1074#1077#1076#1110#1090#1100' '#1096#1082#1072#1083#1091' '#1086#1094#1110#1085#1102#1074#1072#1085#1085#1103':'
  end
  object Label10: TLabel
    Left = 48
    Top = 336
    Width = 239
    Height = 13
    Caption = '4. '#1050#1086#1084#1072#1085#1076#1072' '#1087#1088#1080#1108#1076#1085#1072#1085#1085#1103' '#1084#1072#1108' '#1085#1072#1089#1090#1091#1087#1085#1080#1081' '#1074#1080#1075#1083#1103#1076':'
  end
  object Label11: TLabel
    Left = 192
    Top = 8
    Width = 366
    Height = 19
    Caption = #1058#1077#1089#1090#1091#1074#1072#1085#1085#1103' '#1087#1086' '#1090#1077#1084#1110' "'#1042#1080#1082#1086#1088#1080#1089#1090#1072#1085#1085#1103' '#1079#1072#1087#1080#1089#1110#1074'"'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 48
    Top = 440
    Width = 174
    Height = 13
    Caption = '5. '#1047#1072#1087#1080#1089' '#1079#1072#1076#1072#1085#1086' '#1085#1072#1089#1090#1091#1087#1085#1080#1084' '#1095#1080#1085#1086#1084':'
  end
  object Label8: TLabel
    Left = 48
    Top = 592
    Width = 235
    Height = 13
    Caption = #1071#1082#1110' '#1090#1080#1087#1080' '#1079#1091#1089#1090#1088#1110#1095#1072#1102#1090#1100#1089#1103' '#1091' '#1087#1086#1083#1103#1093' '#1094#1100#1086#1075#1086' '#1079#1072#1087#1080#1089#1091'?'
  end
  object GroupBox1: TGroupBox
    Left = 40
    Top = 613
    Width = 281
    Height = 92
    TabOrder = 0
    object CheckBox1: TCheckBox
      Left = 3
      Top = 18
      Width = 97
      Height = 17
      Caption = #1062#1110#1083#1080#1081
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 3
      Top = 41
      Width = 150
      Height = 17
      Caption = #1044#1110#1081#1089#1085#1080#1081
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 3
      Top = 64
      Width = 150
      Height = 17
      Caption = #1052#1072#1089#1080#1074
      TabOrder = 2
    end
    object CheckBox4: TCheckBox
      Left = 104
      Top = 16
      Width = 97
      Height = 17
      Caption = #1047#1072#1087#1080#1089
      TabOrder = 3
    end
    object CheckBox5: TCheckBox
      Left = 104
      Top = 40
      Width = 97
      Height = 17
      Caption = #1060#1072#1081#1083
      TabOrder = 4
    end
    object CheckBox6: TCheckBox
      Left = 104
      Top = 64
      Width = 97
      Height = 17
      Caption = #1052#1085#1086#1078#1080#1085#1072
      TabOrder = 5
    end
    object CheckBox7: TCheckBox
      Left = 200
      Top = 16
      Width = 73
      Height = 17
      Caption = #1051#1086#1075#1110#1095#1085#1080#1081
      TabOrder = 6
    end
    object CheckBox8: TCheckBox
      Left = 200
      Top = 40
      Width = 73
      Height = 17
      Caption = #1044#1110#1072#1087#1072#1079#1086#1085
      TabOrder = 7
    end
    object CheckBox9: TCheckBox
      Left = 200
      Top = 64
      Width = 73
      Height = 17
      Caption = #1056#1103#1076#1082#1086#1074#1080#1081
      TabOrder = 8
    end
  end
  object RadioGroup1: TRadioGroup
    Left = 48
    Top = 79
    Width = 329
    Height = 74
    Items.Strings = (
      #1047#1073#1077#1088#1110#1075#1072#1090#1080' '#1074' '#1086#1076#1085#1110#1081' '#1079#1084#1110#1085#1085#1110#1081' '#1076#1072#1085#1110' '#1088#1110#1079#1085#1080#1093' '#1090#1080#1087#1110#1074
      #1047#1073#1077#1088#1110#1075#1072#1090#1080' '#1074' '#1086#1076#1085#1110#1081' '#1079#1084#1110#1085#1085#1110#1081' '#1073#1072#1075#1072#1090#1086' '#1088#1110#1079#1085#1080#1093' '#1076#1072#1085#1080#1093' '#1086#1076#1085#1086#1075#1086' '#1090#1080#1087#1091
      #1047#1073#1077#1088#1110#1075#1072#1090#1080' '#1091' '#1079#1084#1110#1085#1085#1110#1081' '#1090#1110#1083#1100#1082#1110' '#1090#1077#1082#1089#1090#1086#1074#1110' '#1076#1072#1085#1110' '#1074#1077#1083#1080#1082#1080#1093' '#1086#1073#1089#1103#1075#1110#1074)
    TabOrder = 1
  end
  object RadioGroup2: TRadioGroup
    Left = 48
    Top = 353
    Width = 265
    Height = 72
    Items.Strings = (
      'with <'#1110#1084#39#1103' '#1079#1084#1110#1085#1085#1086#1111' '#1090#1080#1087#1091' '#1079#1072#1087#1080#1089'> do <'#1082#1086#1084#1072#1085#1076#1072'>;'
      'while <'#1110#1084#39#1103' '#1079#1084#1110#1085#1085#1086#1111' '#1090#1080#1087#1091' '#1079#1072#1087#1080#1089'> do <'#1082#1086#1084#1072#1085#1076#1072'>;'
      'with <'#1082#1086#1084#1072#1085#1076#1072'> do <'#1110#1084#39#1103' '#1079#1084#1110#1085#1085#1086#1111' '#1090#1080#1087#1091' '#1079#1072#1087#1080#1089'>;')
    TabOrder = 2
  end
  object RadioGroup3: TRadioGroup
    Left = 43
    Top = 186
    Width = 185
    Height = 79
    Items.Strings = (
      #1047#1084#1110#1085#1085#1086#1111
      #1053#1077#1090#1080#1087#1110#1079#1086#1074#1072#1085#1086#1111' '#1082#1086#1085#1089#1090#1072#1085#1090#1080
      #1058#1080#1087#1110#1079#1086#1074#1072#1085#1086#1111' '#1082#1086#1085#1089#1090#1072#1085#1090#1080)
    TabOrder = 3
  end
  object Edit1: TEdit
    Left = 48
    Top = 304
    Width = 121
    Height = 21
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    Text = #1042#1074#1077#1076#1110#1090#1100' '#1074#1110#1076#1087#1086#1074#1110#1076#1100
    OnClick = Edit1Click
  end
  object Button1: TButton
    Left = 463
    Top = 660
    Width = 75
    Height = 25
    Caption = #1054#1050
    TabOrder = 5
    OnClick = Button1Click
  end
  object Edit2: TEdit
    Left = 610
    Top = 58
    Width = 47
    Height = 21
    TabOrder = 6
    Text = '5'
  end
  object Button2: TButton
    Left = 552
    Top = 660
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1080
    TabOrder = 7
    OnClick = Button2Click
  end
end
