unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls;

type
  TForm1 = class(TForm)
    Timer1: TTimer;
    procedure FormPaint(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
{    procedure TForm1.Timer1Timer(Sender: TObject);   }
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  x,y,j,p:integer;


implementation

{$R *.dfm}
procedure kadr1(x,y: integer; color: TColor);
  const dx = 10; dy = 10;
  var buf: TColor;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    {���� ����}
    MoveTo(x,y);
    LineTo(x-3*dx,y+4*dy); // ����� ����
    //LineTo(x-2*dx,y+4*dy);  //������
    MoveTo(x,y);
    LineTo(x+3*dx,y+4*dy);   //�������
    //LineTo(x+4*dx,y+4*dy);   //������
    MoveTo(x,y);
    LineTo(x,y-5*dy);      //���
    {���� ����}
    MoveTo(x,y-4*dy);      //������ ���
    LineTo(x-3*dx,y-4*dy-3*dy);  //� ����
    MoveTo(x,y-4*dy);
    LineTo(x+3*dx,y-4*dy-3*dy);   //� ����
    MoveTo(x,y-5*dy);
    Ellipse(x-10,y-5*dy-20,x+10,y-5*dy);

    MoveTo(x-9*dx,y-4*dy-3*dy);
    LineTo(x-9*dx+18*dx,y-4*dy-3*dy);
    MoveTo(x-9*dx,y-4*dy-3*dy);
    LineTo(x-9*dx,y+8*dy);
    MoveTo(x-9*dx+18*dx,y-4*dy-3*dy);
    LineTo(x-9*dx+18*dx,y+8*dy);
    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr2(x,y: integer; color: TColor);
  const dx = 10; dy = 10;
  var buf: TColor;
    y1:integer;
begin
 with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    y1:=y;
    y:=y-dy;
    {���� ����}
    MoveTo(x,y);
    LineTo(x-3*dx,y+4*dy); // ����� ����
    //LineTo(x-2*dx,y+4*dy);  //������
    MoveTo(x,y);
    LineTo(x+3*dx,y+4*dy);   //�������
    //LineTo(x+4*dx,y+4*dy);   //������
    MoveTo(x,y);
    LineTo(x,y-5*dy);      //���
    {���� ����}
    MoveTo(x,y-4*dy);      //������ ���
    LineTo(x-2*dx,y-4*dy);  //� ����
    LineTo(x-2*dx-5,y-4*dy-2*dy);  //� ����
    MoveTo(x,y-4*dy);
    LineTo(x+2*dx,y-4*dy);   //� ����
    LineTo(x+2*dx+5,y-4*dy-2*dy);  //� ����
    MoveTo(x,y-5*dy);
    Ellipse(x-10,y-5*dy-20,x+10,y-5*dy);

    y:=y1;
    MoveTo(x-9*dx,y-4*dy-3*dy);
    LineTo(x-9*dx+18*dx,y-4*dy-3*dy);
    MoveTo(x-9*dx,y-4*dy-3*dy);
    LineTo(x-9*dx,y+8*dy);
    MoveTo(x-9*dx+18*dx,y-4*dy-3*dy);
    LineTo(x-9*dx+18*dx,y+8*dy);

    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure kadr3(x,y: integer; color: TColor);
const dx = 10; dy = 10;
  var buf: TColor;
  y1:integer;
begin
with form1.canvas do begin
    buf:=pen.Color; // �������� ������ ����
    pen.Color:=color; // ������������ �������� ����
    y1:=y;
    y:=y-2*dy;
    {���� ����}
    MoveTo(x,y);
    LineTo(x-3*dx,y+4*dy); // ����� ����
    //LineTo(x-2*dx,y+4*dy);  //������
    MoveTo(x,y);
    LineTo(x+3*dx,y+4*dy);   //�������
    //LineTo(x+4*dx,y+4*dy);   //������
    MoveTo(x,y);
    LineTo(x,y-5*dy);      //���
    {���� ����}
    MoveTo(x,y-4*dy);      //������ ���
    LineTo(x-2*dx,y-4*dy+6);  //� ����
    LineTo(x-2*dx+4,y-4*dy-dy);  //� ����
    MoveTo(x,y-4*dy);
    LineTo(x+2*dx,y-4*dy+6);   //� ����
    LineTo(x+2*dx-4,y-4*dy-dy);  //� ����
    MoveTo(x,y-5*dy);
    Ellipse(x-10,y-5*dy-20,x+10,y-5*dy);

    y:=y1;
    MoveTo(x-9*dx,y-4*dy-3*dy);
    LineTo(x-9*dx+18*dx,y-4*dy-3*dy);
    MoveTo(x-9*dx,y-4*dy-3*dy);
    LineTo(x-9*dx,y+8*dy);
    MoveTo(x-9*dx+18*dx,y-4*dy-3*dy);
    LineTo(x-9*dx+18*dx,y+8*dy);

    pen.Color:=buf; // ���������� ������ ���� �����
    end;
end;

procedure TForm1.Timer1Timer(Sender: TObject);
begin
if j<3 then j:=j+1
  else j:=1;

case j of              //������� ������� ����
1:  kadr3(x,y,form1.color);
2:  kadr1(x,y,form1.color);
3:  kadr2(x,y,form1.color);
end; //case

case j of       //���������� ��������� ����
  1: kadr1(x,y,clWhite);
  2: kadr2(x,y,clWhite);
  3: kadr3(x,y,clWhite);
end; //case
end;


procedure TForm1.FormPaint(Sender: TObject);
begin
x:=150; y:=100; j:=0;
 Form1.Color:=clNavy;
 Timer1.Interval := 450;
end;

end.
