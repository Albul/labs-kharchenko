unit trykutnyk;
interface
	procedure perumetr(x1,y1,x2,y2,x3,y3:integer; var p:real);
	procedure ploscha(x1,y1,x2,y2,x3,y3:integer; var s:real);
	procedure vysota(x1,y1,x2,y2,x3,y3:integer; var h1,h2,h3:real);
	procedure mediana(x1,y1,x2,y2,x3,y3:integer; var m1,m2,m3:real);

implementation
	procedure perumetr(x1,y1,x2,y2,x3,y3:integer; var p:real);
	var a,b,c:real;
	begin
		a:=sqrt(sqr(x1-x2)+sqr(y1-y2));
		b:=sqrt(sqr(x1-x3)+sqr(y1-y3));
		c:=sqrt(sqr(x2-x3)+sqr(y2-y3));
		p:=a+b+c;
	end;
	
	procedure ploscha(x1,y1,x2,y2,x3,y3:integer; var s:real);
	var a,b,c,p:real;
	begin
		a:=sqrt(sqr(x1-x2)+sqr(y1-y2));
		b:=sqrt(sqr(x1-x3)+sqr(y1-y3));
		c:=sqrt(sqr(x2-x3)+sqr(y2-y3));
		p:=(a+b+c)/2;
		s:=sqrt(p*(p-a)*(p-b)*(p-c));
	end;

	procedure vysota(x1,y1,x2,y2,x3,y3:integer; var h1,h2,h3:real);
	var a,b,c,p:real;	
	begin
		a:=sqrt(sqr(x1-x2)+sqr(y1-y2));
		b:=sqrt(sqr(x1-x3)+sqr(y1-y3));
		c:=sqrt(sqr(x2-x3)+sqr(y2-y3));
		p:=(a+b+c)/2;

		h1:=(2/a)*sqrt(p*(p-a)*(p-b)*(p-c));
		h2:=(2/b)*sqrt(p*(p-a)*(p-b)*(p-c));
		h3:=(2/c)*sqrt(p*(p-a)*(p-b)*(p-c));
	end;

	procedure mediana(x1,y1,x2,y2,x3,y3:integer; var m1,m2,m3:real);
	var a,b,c,p:real;	
	begin
		a:=sqrt(sqr(x1-x2)+sqr(y1-y2));
		b:=sqrt(sqr(x1-x3)+sqr(y1-y3));
		c:=sqrt(sqr(x2-x3)+sqr(y2-y3));
		p:=(a+b+c)/2;
		
		m1:=sqrt(2*b*b+2*c*c-a*a)/2;
		m2:=sqrt(2*a*a+2*c*c-b*b)/2;
		m3:=sqrt(2*a*a+2*b*b-c*c)/2;	
	end;

begin
end.
