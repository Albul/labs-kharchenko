unit longNumber;
  {Nash modul`}

interface      {opus interfeisy}

const LongMaxSize = 500;{ maksumalna kilkist` znakiv v chusli}

type tsifra = 0..9;
     chislo = array [1..LongMaxSize] of tsifra;     {tup dovgogo chusla}
     long = chislo;
     digit = tsifra;

procedure LongError;            {procedura dlya vuznachennya pomulku}
procedure LongMakeEmpty(var x : long);   {zapovnennja 4isla 0}
function MaxSize(x, y : long) : integer; {znajti chislo z bilshoju kilkistju znakiv}
function LongSize(x : long) : integer; {vizna4ennja kilkosti znakiv}
procedure LongDevision(x, y : long; var lDiv, lMod : long); {mod ta div}

procedure LongRead(var x : long);
procedure LongReadLn(var x : long);
procedure LongWrite(x : long);
procedure LongWriteLn(x : long);


procedure LongSum(x, y : long; var res : long);  {procedura znaxpdjennya symu}
procedure LongMinus(x, y : long; var res : long);   {procedura znaxpdjennya riznuci}
procedure LongMultiply(x, y : long; var res : long);  {procedura znaxodjennya symu}
procedure LongDiv(x, y : long; var res : long);       {procedura delennya}
procedure LongMod(x, y : long; var res : long);    {procedura znaxpdjennya zalushky vid dilennya}
{========= fynkcii vuznachennya nerivnisti chusel =============}
function LongEqual(x, y : long) : boolean;
function LongNotEqual(x, y : long) : boolean;
function LongBiggerOrEqual(x, y : long) : boolean;
function LongBigger(x, y : long) : boolean;
function LongSmallerOrEqual(x, y : long) : boolean;
function LongSmaller(x, y : long) : boolean;

implementation
 {======= procedura dlya vuvedennya povidomlennya pro pomulky ========}
procedure LongError;
begin
  Write('Operaciy nemojluvo vukonatu ');
  readln;
  readln;
  Halt(0);
end;
{=======zapovnennja 4isla 0============}
procedure LongMakeEmpty(var x : long);
var i : integer;
begin
  FillChar(x, LongMaxSize, 0);
end;

{===== znajti chislo z bilshoju kilkistju znakiv  =========}
function MaxSize(x, y : long) : integer;
begin
  if LongSize(x) < LongSize(y) then
    MaxSize := LongSize(y)
  else
    MaxSize := LongSize(x);
end;

function LongSize(x : long) : integer; {znajti kilkist znakiv}
var i : integer;
begin
  for i := LongMaxSize downto 1 do
    if x[i] > 0 then
      break;
  LongSize := i;
end;

{===== div ta mod =====}
procedure LongDevision(x, y : long; var lDiv, lMod : long);
var one, tmp : long;
begin
  if LongSmaller(x, y) then {nemozhlivo vikonati}
  begin
    LongError;
    exit;
  end;
  LongMakeEmpty(lDiv);
  LongMakeEmpty(lMod);
  LongMakeEmpty(one);
  if (LongSize(y) = 1) AND (y[1] = 1) then  {jaksho dilemo na 1}
  begin
    lDiv := x;
    exit;
  end;
  if (LongSize(y) <= 1) AND (y[1] = 0) then  {jaksho dilemo na 0}
  begin
    LongError;
    exit;
  end;
  one[1] := 1;
  while LongBiggerOrEqual(x, y) do  {vidnimajemo, poki moshlivo}
  begin
    LongSum(lDiv, one, tmp);
    lDiv := tmp;                    {tsila 4astina}
    LongMinus(x, y, tmp);
    x := tmp;
  end;
  lMod := x;                        {osta4a}
end;

procedure LongRead(var x : long);
var ch : char;
    i, k : integer;
    tmp : digit;
begin
  LongMakeEmpty(x) ;
  Read(ch);
  While Not(ch In ['0'..'9']) Do Read(ch);  {propusk ne 4isel}
  i := 1;
  While ch In ['0'..'9'] Do             {4utaemo vsi tsifri}
  Begin
    x[i] := Ord(ch) - Ord('0');         {viznachaemo tsifru}
    Read(ch);
    Inc(i);
  End;
  for k := 1 to (i - 1) div 2 do        {perevertaemo 4islo}
  begin
    tmp := x[k];
    x[k] := x[i - k];
    x[i - k] := tmp;
  end;
End;

procedure LongReadLn(var x : long);
begin
  LongRead(x);
  WriteLn;
end;


procedure LongWrite(x : long);
var i: integer;
begin
  for i := LongSize(x) downto 1 do
    Write(x[i]);
end;

procedure LongWriteLn(x : long);
begin
  LongWrite(x);
  WriteLn;
end;

procedure LongSum(x, y : long; var res : long); {sumujemo jak v "stovb4ik"}
var i, tmpSum : integer;
    carry : digit;
begin
  LongMakeEmpty(res);
  carry := 0;             {jaksho perepovnolos: (9 + 1)}
  for i := 1 to MaxSize(x, y) do
  begin
    tmpSum := x[i] + y[i] + carry;
    res[i] := tmpSum mod 10;        {tsifra}
    carry := tmpSum div 10;         {perepovnennja}
  end;
  if carry > 0 then
    if i < LongMaxSize then         {rezultat nadto velikij}
      res[i + 1] := carry
    else
      LongError;
end;

procedure LongMinus(x, y : long; var res : long); {vidnimajemo jak v "stovb4ik"}
var i, tmpSum, carry : integer;
begin
  if LongSmaller(x, y) then     {vihodit vedjemne 4islo}
  begin
    LongError;
    exit;
  end;
  LongMakeEmpty(res);
  if LongBigger(x, y) then
  begin
    carry := 0;
    for i := 1 to LongSize(x) do
    begin
      tmpSum := x[i] - y[i] - carry + 10 * ord(x[i] - carry < y[i]);
      res[i] := tmpSum mod 10;                     {tsivra}
      carry := ord(x[i] - carry < y[i]);         {prosimo 1 u starshogo rozrjadu}
    end;
  end;
end;
 {======= procedura mnojennya chusel ==========}
procedure LongMultiply(x, y : long; var res : long);
var i, k, carry, tmpSum : integer;              {lokal`ni zminni}
begin
  LongMakeEmpty(res);
  if LongSize(x) + LongSize(y) > LongMaxSize then  {yaksho rezyltat ne nalejut` nashomy diapazony}
  begin
    LongError;  {vuklukatu procedury obroblennya pomulok}
    exit;
  end;
  for i := 1 to LongSize(x) do       {mnojennya velukux chusel  }
  begin
    carry := 0;
    for k := 1 to LongSize(y) do
    begin
      tmpSum := x[i] * y[k] + res[i + k - 1] + carry;
      carry := tmpSum div 10;
      res[i + k - 1] := tmpSum - 10 * carry;
    end;
    res[i + k] := res[i + k] + carry;
  end;
end;
 {======== procedura dilennya =============}
procedure LongDiv(x, y : long; var res : long);
var tmp : long;
begin
  LongDevision(x, y, res, tmp);
end;

procedure LongMod(x, y : long; var res : long);
var tmp : long;
begin
  LongDevision(x, y, tmp, res);
end;

function LongEqual(x, y : long) : boolean;
var i: integer;
begin
  if LongSize(x) = LongSize(y) then
  begin
    LongEqual := true;
    for i := 1 to LongMaxSize do
      if NOT (x[i] = y[i]) then
      begin
        LongEqual := false;
        break;
      end;
  end
  else
    LongEqual := false;
end;
{=========== funkcii znaxodjennya nerivnosti ================}
function LongNotEqual(x, y : long) : boolean;
begin
  LongNotEqual := NOT LongEqual(x, y);
end;

function LongBiggerOrEqual(x, y : long) : boolean;
begin
  LongBiggerOrEqual := NOT LongSmaller(x, y);
end;

function LongBigger(x, y : long) : boolean;
begin
  LongBigger := (NOT LongSmaller(x, y)) AND (NOT LongEqual(x, y));
end;

function LongSmallerOrEqual(x, y : long) : boolean;
begin
  LongSmallerOrEqual := LongSmaller(x, y) OR LongEqual(x, y);
end;

function LongSmaller(x, y : long) : boolean;
var i : integer;
begin
  if LongSize(x) > LongSize(y) then   {perevirka rozmiriv}
    LongSmaller := false
  else
    if LongSize(x) < LongSize(y) then
      LongSmaller := true
    else
    begin
      i := LongSize(x);
      while (i > 0) AND (x[i] = y[i]) do  {perevirjaemo kozhne 4islo}
        Dec(i);
      if i = 0 then
        LongSmaller := false
      else
        LongSmaller := x[i] < y[i];
    end;
end;

end.
