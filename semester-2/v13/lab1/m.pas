uses longNumber;    {pidkluchaemo modul}
{Osnovna programma}
var x, y, tmp : long;     {zminni tupy dovgux chusel}
z : array [1..100] of long;   {Tup dovgux chusel}
    i, n : integer;

    {========= procedura pidnesennja v stepin =========}
    procedure LongPower(x, y : long; var res : long);       {pidnesennja v stepin}
    var one, tmpSum, tmp : long;
    begin
      LongMakeEmpty(res);
      if (LongSize(y) <= 1) AND (y[1] = 0) then  {jaksho stepin = 0}
      begin
        res[1] := 1;
        exit;
      end;
      if (LongSize(x) = 1) AND (x[1] = 1) then   {jaksho 4islo = 1}
      begin
        res[1] := 1;
        exit;
      end;
      if (LongSize(x) <= 1) AND (x[1] = 0) then  {jakshi stepin = 1}
        exit;
      LongMakeEmpty(one);
      LongMakeEmpty(tmp);
      LongMakeEmpty(tmpSum);
      one[1] := 1;
      res := one;
      while (LongSize(y) > 1) OR (y[1] > 0) do  {y raz pomnoshiti 4islo x}
      begin
        LongMultiply(res, x, tmpSum);
        res := tmpSum;
        LongMinus(y, one, tmp);         {zmenshiti kilkist raziv}
        y := tmp;
      end;
    end;
     {=========== Procedura sortyvannya ==========}
    procedure LongSort(var x : array of long; n : integer);
    var i, k : integer;  {lokalni zminni }
        tmp : long;
    begin
      LongMakeEmpty(tmp);     {vukluk proceduru dlya perevirku znachen` argymentiv}
      for i := 0 to n - 1 do
        for k := 0 to n - 1 do
          if LongSmaller(x[k], x[k + 1]) then
          begin
            tmp := x[k];
            x[k] := x[k + 1];
            x[k + 1] := tmp;
          end;

    end;
{========= procedura vuvedennya chusel =========}
    procedure output(x : array of long; n : integer);
    var i : integer;
    begin
      for i := 0 to n - 1 do
        LongWriteLn(x[i]);       {vukluk proceduru vvuvedennya odnogo chusla }
    end;
 {=================== pochatok golovnoi chastunu ====================}
begin
  Writeln('Maksumalna dovjina chusla = 1000 sumvoliv.');
  longMakeempty(x); {perevidrka znachennya zminnoi x}
  longmakeempty(y);  {perevidrka znachennya zminnoi y}
  writeln('input x:');
  Longreadln(x);       {Vuklux proceduru dlya vvedennya dovgogo chusla x}
  writeln('input y:');
  longreadln(y);       {Vukluk proceduru dlya vvedennya dovgogo chusla y}

  write('x = ');
  LongWriteln(x); {vuvestu chuslo x}
  write('y = ');
  LongWriteLn(y);  {vuvestu chuslo y}
  {======== operacii porivnyyannya ===========}
  writeln('x < y = ', LongSmaller(x, y));
  writeln('x <= y = ', LongSmallerOrEqual(x, y));
  writeln('x = y = ', LongEqual(x, y));
  writeln('x <> y = ', LongNotEqual(x, y));
  writeln('x > y = ', LongBigger(x, y));
  writeln('x >= y = ', LongBiggerOrEqual(x, y));
  write('x + y = ');
  LongSum(x, y, tmp); {Vukluk proceduru dlya obchuslennya symu chusel}
  LongWriteLn(tmp);     {Vukluk proceduru vuvedennya rezyl`taty}

   write('x - y = ');
  LongMinus(x, y, tmp);  {Vukluk proceduru vidnimannya}
  LongWriteLn(tmp);       {Vukluk proceduru vuvedennya rezyl`taty}
  write('x * y = ');
  LongMultiply(x, y, tmp);
  LongWriteLn(tmp);        {Vukluk proceduru vuvedennya rezyl`taty}
  write('x ^ y = ');
  Longpower(x, y, tmp);   {Vukluk proceduru znaxodjennya stepennya}
  LongWriteLn(tmp);       {Vukluk proceduru vuvedennya rezyl`taty}
  write('x / y = ');
  LongDiv(x, y, tmp);    {Vukluk priceduru dilennya }
  LongWriteLn(tmp);     {Vukluk proceduru vuvedennya rezyl`taty}
  write('x % y = ');
  LongMod(x, y, tmp);    {Vukluk proceduru znaxpdjennya ostachi}
  LongWriteLn(tmp);     {Vukluk proceduru vuvedennya rezyl`taty}

  writeln('vvedite koli4estvo 4isel dlja sortirovki < 100:');
  readln(n);                   {kilkist` chisel dlya sortyvannya}
  writeln('vvedite 4isla:');
  for i := 1 to n do       {vvedenya dovgux chusel dlya sortvannya}
  begin
    Longreadln(z[i]);    {vvedenya dovgux chusel dlya sortvannya}
  end;
  writeln('unsorted:');
  output(z, n);         {vukluk proceduru vuvedennya  nevidsortovanux chusel}
  longsort(z, n);       {vukluk proceduru sortyvannya}
  writeln('sorted:');
  output(z, n);           {vukluk proceduru vuvedennya vidsortovanux chusel}
  readln;
  readln;
end.
