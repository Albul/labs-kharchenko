uses graph;
var er,grd,grm:integer;
begin
        grd:=detect;
        initgraph(grd,grm,'');
        er:=graphresult;
        if er<>grok then begin
                writeln('graph error');
                halt(er);
        end;
        setlinestyle(3,1,1);
        line(450,400,490,368);
        setlinestyle(0,1,1);
        line(450,400,600,400);
        line(600,400,640,368);
        setlinestyle(3,1,1);
        line(490,368,640,368);

        setlinestyle(0,1,1);
        line(450,200,490,168);
        line(450,200,600,200);
        line(600,200,640,168);
        line(490,168,640,168);

        line(450,400,450,200);
        setlinestyle(3,1,1);
        line(490,368,490,168);

        setlinestyle(0,1,1);
        line(600,200,600,400);

        line(640,168,640,368);

        {pobudova pererizu}
        setlinestyle(0,1,3);
        setcolor(2);

        line(450,400,600,325);
        line(490,368,640,292);

        line(600,325,640,292);
        line(450,400,490,368);

        setfillstyle(2,2);
        floodfill(528,350,2);

        readln;
        closegraph;
end.
