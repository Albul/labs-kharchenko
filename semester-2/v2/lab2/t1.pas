uses graph;
var er,grd,grm:integer;
begin
        grd:=detect;
        initgraph(grd,grm,'');
        er:=graphresult;
        if er<>grok then begin
                writeln('graph error');
                halt(er);
        end;

        {nozhka}
        line(350,450,350,350);
        line(360,450,360,350);

        {osnova}
        line(350,450,320,460);
        line(360,450,390,460);
        line(320,460,390,460);

        {emkist"}
        line(320,350,390,350);
        line(320,350,295,220);
        line(390,350,420,220);


        ellipse(357,220,180,360,62,20);


        {krugi}
        arc(340,200,38,352,33);
        arc(390,230,0,210,18);
        circle(392,185,27);

        {zalyvka bokala}
        setfillstyle(1,1);
        floodfill(351,430,15);
        floodfill(323,250,15);


        readln;
        closegraph;
end.
