program piram;
uses crt,graph;
var
	driver,mode:integer;
begin
	driver:=detect;
	initgraph(driver,mode,'');
        {osnova}
	line(400,400,600,450);
	line(600,450,650,350);
        setlinestyle(3,0,1);
	line(650,350,400,400);

        setlinestyle(0,1,1);

        {rebra}
	line(450,150,400,400);
	line(450,150,600,450);
	line(450,150,650,350);

        {pereriz}
	setcolor(2);
        line(600,450,525,375);
        line(600,450,595,295);
        line(525,375,595,295);

        {zalyvka pererizu}
	setfillstyle(3,2);
	floodfill(590,430,2);
	setcolor(15);

        {navodymo liniyu}
	line(450,150,600,450);

	readln;
	closegraph;
end.
