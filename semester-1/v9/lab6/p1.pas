const n=10;
var a:array[1..n] of integer;
	i,j,d,min:integer;
begin
	
	
	randomize;
	for i:=1 to n do
                if (i mod 2) = 0 then
	                a[i]:=random(n)
                else
                        a[i] := random(-n);

	writeln('Input array:');
	for i:=1 to n do
		write(a[i],' ');

	writeln;

	min:=a[1];

	for i:=1 to n do
		if a[i]<min then
			min:=a[i];

	for i:=1 to n do
		if a[i]>0 then
			a[i]:=min;


	writeln('Output array:');
	for i:=1 to n do
		write(a[i],' ');

	writeln;
	readln;
end.
