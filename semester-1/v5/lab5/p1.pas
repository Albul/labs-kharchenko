var k,d1,d2,p1,p2:longint;

procedure AddRightDigit(k,d:longint; var k1:longint);
var s,s1:string;
	pom:integer;
begin
	str(k,s);
	str(d,s1);
	insert(s1,s,length(s)+1);
	val(s,k1,pom);
end;

begin
	writeln('Enter a number: k>0, and digits: d1,d2=0..9 :');
	readln(k,d1,d2);
	AddRightDigit(k,d1,p1);
	AddRightDigit(p1,d2,p2);
	writeln('Number ',k,' + digit ',d1,' = ',p1);
	writeln('Number ',p1,' + digit ',d2,' = ',p2);
	readln;
end.
