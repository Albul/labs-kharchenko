var a1,h1,a2,h2,a3,h3:real;

function triangleP(a,h:real):real;
var a1:real;
begin
	a1:=a/2;
	triangleP:=2*sqrt(sqr(h)+sqr(a1))+a;
end;


begin
	writeln('Enter basis of a1 and height of h1 triangle:');
	readln(a1,h1);

	writeln('Perimeter of isosceles triangle with basis of ',a1:5:2,' and height of ',h1:5:2,' = ',triangleP(a1,h1):5:2);

	writeln('Enter basis of a2 and height of h2 triangle:');
	readln(a2,h2);
	writeln('Perimeter of isosceles triangle with basis of ',a2:5:2,' and height of ',h2:5:2,' = ',triangleP(a2,h2):5:2);

	writeln('Enter basis of a3 and height of h3 triangle:');
	readln(a3,h3);
	writeln('Perimeter of isosceles triangle with basis of ',a3:5:2,' and height of ',h3:5:2,' = ',triangleP(a3,h3):5:2);

	readln;
end.
