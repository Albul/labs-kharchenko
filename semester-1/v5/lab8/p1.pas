const n=5;
var f,g:file of real;
	i,k:integer;
	j,max:real;
begin
	assign(f,'input_z1');
	assign(g,'output_z1');

	rewrite(f);

	randomize;
	for i:=1 to n do
		write(f,(random(n)+(i*0.1)));

	close(f);


	reset(f);
	rewrite(g);

	max:=0;

	writeln('Input file:');
	while not(eof(f)) do begin
		read(f,j);
		if max<j then max:=j;
		write(j:5:2,' ');
	end;
	writeln;

	write(g,max);

	close(f);
	close(g);

	reset(g);

	writeln('Output file:');
	while not(eof(g)) do begin
		read(g,j);
		write(j:5:2,' ');
	end;
	writeln;
	close(g);
	readln;
end.