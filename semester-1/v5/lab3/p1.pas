var h,x,a,b,m,y:real;
	i:integer;
begin
	writeln('Enter a,b,m:');
	readln(a,b,m);

	h:=(b-a)/m;
	i:=0;
	x:=a;

	while (a+i*h) <= b do begin
		x:=a+i*h;
		y:=exp(-x)+cos(3*x);
		i:=i+1;
		writeln('x=',x:5:3,' f(x)=',y:10:5);
	end;

	readln;
end.
