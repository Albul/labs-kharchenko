program pro2;
var n1,n2,n3:integer;
function calc(a,b:real; op:integer):real;
begin
	case op of
		1 : calc:=a-b;
		2 : calc:=a*b;
		3 : calc:=a/b;
		else 
			calc:=a+b
	end;
end;


begin
	writeln('Enter integers N1<>0,N2<>0,N3<>0:');
	readln(n1,n2,n3);
	writeln(n1,' - ',n2,' = ',calc(n1,n2,1):5:2);
	writeln(n1,' * ',n2,' = ',calc(n1,n2,2):5:2);
	writeln(n1,' / ',n2,' = ',calc(n1,n2,3):5:2);
	writeln(n1,' + ',n3,' = ',calc(n1,n3,56):5:2);
	
	readln;
end.