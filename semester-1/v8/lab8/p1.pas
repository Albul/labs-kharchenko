program lr8_1;

const k=5;
var f,g:file of real;
i,j:integer;
r,max:real;
fname,gname:string[15];

begin
	j:=0;
	max:=0;
	writeln('Enter the name input file:');
	readln(fname);
	writeln('Enter the name output file:');
	readln(gname);

	assign(f,fname);
	assign(g,gname);
	rewrite(f);
	
	writeln('Enter 5 number:');
	for i:=1 to k do begin
		read(r);
		write(f,r);
	end;

	reset(f);
	rewrite(g);

	while not EOF(f) do begin
		read(f,r);
		j:=j+1;
        	if odd(j)=true then
        		if max<abs(r) then max:=r;
	end;

	write(g,max);

	reset(g);
	while not eof(g) do begin
        	read(g,r);
        	writeln(r:0:5);
	end;

	close(f);
	close(g);

	readln;
	readln;
end.