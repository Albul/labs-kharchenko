program lr6_2;
const n=3;
m=3;
var a:array[1..m,1..n] of integer;
i,j,id,idm,min,max,x:integer;
begin
	writeln('Enter array:');
	for i:=1 to n do
		for j:=1 to m do
           		read(a[i,j]);

	for i:=1 to n do begin
		max:=a[i,1]; id:=1;
		min:=a[i,1]; idm:=1;
       		for j:=2 to m do begin
			if max<a[i,j] then begin
				max:=a[i,j];
				id:=j;
			end;
			if min>a[i,j] then begin
				min:=a[i,j];
				idm:=j;
			end;
		end;
		x:=a[i,id]; 
		a[i,id]:=a[i,idm]; 
		a[i,idm]:=x;
	end;

	for i:=1 to n do begin
		writeln;
		for j:=1 to m do
			write(a[i,j]);
	end;

	writeln;
	readln;
	readln;
end.