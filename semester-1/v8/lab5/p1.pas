program lr5_1;
uses crt;
var a,j:int64;

procedure InvertDigits(k:int64);
var i,l:integer;
begin
    while k<>0 do begin
    	l:=k mod 10;
    	write(l);
    	k:=k div 10;
    end;

    writeln;
end;

begin
	clrscr;
	j:=1;
        writeln('Enter 5 numbers:');
        readln(a);
	while j<=5 do begin
		write('Reverse order of numbers: ');
		InvertDigits(a);
		readln(a);
		j:=j+1;
	end;

	readln;
end.
