type mytype=^student;
        student=record
        prizv:string;
        next:mytype;
        pred:mytype;
        end;
        prst=string[20];

var spis,fs:mytype;
        st,s1:prst;
        n:word;

{Процедура створення списку}
procedure dodvspis(var lst:mytype; nov:prst);
        var p,z:mytype;
        begin
        new(z);
        z^.pred:=nil;
        z^.next:=nil;
        z^.prizv:=nov;
        if lst=nil then
                begin
                lst:=z;
                end
                else
                begin
                p:=lst;
                while p^.next<>nil do p:=p^.next;
                p^.next:=z;
                z^.pred:=p;
                end;
        end;

{Процедура знищення елемента списку}
procedure vitirlan(var l:mytype; d:prst);
        var q:mytype;
        begin
        if l=nil then writeln('Ланка ',d,' відсутня')
        else
        if l^.prizv<>d then vitirlan(l^.next,d)
                else
                      begin
                      q:=l;
		      l^.next^.pred:=l^.pred^.pred;
		      l:=l^.next;
		     	{
		      l^.pred^.next:=l^.next;
		      l^.next^.pred:=l^.pred;
		      }
                      dispose(q);
                      end;

        end;

{Процедура виводу списку}
procedure list_druk(lst:mytype);
        begin
        if lst<>nil then
                begin
                writeln(lst^.prizv);
                list_druk(lst^.next);
                end;

        end;


{Процедура виводу списку у зворотньому напрямку}
procedure list_druk_modif(lst:mytype);
        begin
        if lst^.next<>nil then list_druk_modif(lst^.next);
        writeln(lst^.prizv);
        end;


{Процедура вставлення елемента у список}
procedure vstavlanku(var l:mytype; x,d:prst);
var q:mytype;
        begin
        if l=nil then writeln('Елемент',x,' не знайдено')
                else
                if l^.prizv<>x then vstavlanku(l^.next,x,d)
                        else
                        begin
                        new(q);
                        q^.prizv:=d;
                        q^.next:=l^.next;
                        l^.next:=q;
                        q^.pred:=l;
                        l^.next^.pred:=q;
                        end;


        end;

{Процедура пошуку елемента у списку}
function poshukelem(l:mytype; d:prst; var nom:word):boolean;
        var f:boolean;
                i:word;
        begin
        poshukelem:=false;
        nom:=0;
        i:=0;
        f:=true;
        repeat
                if l=nil then f:=false
                        else
                                if l^.prizv<>d then l:=l^.next
                                        else
                                        begin
                                        poshukelem:=true;
                                        nom:=i+1;
                                        f:=false;
                                        end;
                i:=i+1;
                until f=false

        end;



begin

spis:=nil;

writeln('Введіть прізвище студента:');
writeln('Кінець введення - 000');
readln(st);
while st<>'000' do
        begin
        dodvspis(spis,st);
        writeln('Введіть прізвище студента:');
        writeln('Кінець введення - 000');
        readln(st);
        end;

writeln('Список:');
list_druk(spis);
readln;

writeln('Введіть прізвище для знищення із списку:');
readln(st);
vitirlan(spis,st);
list_druk(spis);


writeln('Введіть прізвище студента яке потрібно вставити:');
readln(s1);
writeln('Введіть прізвище студента після якого вставити:');
readln(st);

vstavlanku(spis,st,s1);
list_druk(spis);

readln;

writeln('Введіть прізвище для пошуку в списку:');
readln(st);

if poshukelem(spis,st,n)=true then writeln('Ланка ',st,' ',n)
        else writeln('Такого елемента в списку немає');

readln;
writeln('Список у зворотньому порядку:');
list_druk_modif(spis);

end.
