const maxn=10000;
type indx=1..maxn;
	{Оголошуємо тип динамічного масиву, елементами якого є цілі числа типу longint}
	art=array of longint;
var j,k,m,i:longint;
	t:text;
	a:art;


begin
{Задаємо довжину динамічного масиву, вона буде рівною maxn}
setlength(a,maxn);
assign(t,'chyslo.txt');
rewrite(t);


{Записуємо в динамічний масив випадкові значення, і зразу кожний елемент масиву записуємо у текстовий файл t. (Номерація динамічного масиву починається з нуля, тому j:=0;)}
j:=0;
randomize;
while j<maxn do
	begin
	a[j]:=random(maxn);
	writeln(t,a[j]);
	j:=j+1;
	end;

close(t);


end.
