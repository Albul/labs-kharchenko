const n=10;
var a:array[1..n] of integer;
	i,j,k,p,nep:integer;
begin
{}
randomize;
for i:=1 to n do
	a[i]:=random(n);

writeln('Input array:');
for i:=1 to n do
	write(a[i],' ');

writeln;

writeln('Even numbers:');
for i:=1 to n do
	if (a[i] mod 2)=0 then write(a[i],' ');

writeln;
writeln('Not even numbers:');
for i:=1 to n do
	if (a[i] mod 2)<>0 then write(a[i],' ');

readln;
end.
