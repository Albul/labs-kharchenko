var
	x1,y1,x2,y2:real;
	d1,d2:real;
begin
	writeln('Enter the co-ordinates of point (x1,y1):');
	readln(x1,y1);

	writeln('Enter the co-ordinates of point (x2,y2):');
	readln(x2,y2);

	d1 := sqrt(x1*x1+y1*y1);
	d2 := sqrt(x2*x2+y2*y2);

	if d1 > d2 then
		writeln('Point #1 nearer to beginning of co-ordinates')
	else
		if (d1 = d2) then
			writeln('Points are on identical distance')
		else
			writeln('Point #2 nearer to beginning of co-ordinates');

	readln;
	readln;
end.
