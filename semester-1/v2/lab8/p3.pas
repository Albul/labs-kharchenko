var f,g:text;
	c,c1,c2:char;
	i,j,k:integer;
	s:string;

begin
assign(f,'input_z3.txt');
assign(g,'output_z3.txt');

reset(f);
rewrite(g);

while not(eof(f)) do begin
	readln(f,s);
	for i:=1 to length(s) do begin
		c:=s[i];
		c1:=s[i+1];
		c2:=s[i+2];
		case c of
			'a'..'z' : begin
				case c1 of 
					'+','-','*','/' : begin
						case c2 of 
							'a'..'z' : writeln(g,copy(s,i,3));
						end;
					end;
				end;
			end;
		end;
	end;
end;

close(f);
close(g);

readln;
end.