var
	a, b, c:integer;

function RootsCount(a, b, c:real):integer;
var
	d:real;
begin
	d := b*b - 4 * a * c;

	if d < 0 then
		RootsCount := 0;
	
	if d = 0 then
		RootsCount := 1;

	if d > 0 then
		RootsCount := 2;
end;

begin
	writeln('Enter coefficients a1, b1, c1');
	readln(a, b, c);
	writeln('Amount of roots = ',RootsCount(a, b, c));

	writeln('Enter coefficients a2, b2, c2');
	readln(a, b, c);
	writeln('Amount of roots = ',RootsCount(a, b, c));

	writeln('Enter coefficients a3, b3, c3');
	readln(a, b, c);
	writeln('Amount of roots = ',RootsCount(a, b, c));

	readln;
end.
